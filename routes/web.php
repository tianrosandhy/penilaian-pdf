<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//route dashboard dan login
Route::group(['prefix' => env('ADMIN_PREFIX')], function($route){
	$route->get('/', 'Admin\\DashboardController@index')->name('admin.dashboard');
	$route->get('login', 'Admin\\DashboardController@login')->name('admin.login');
	$route->post('login', 'Admin\\DashboardController@loginProcess')->name('admin.postlogin');
	$route->get('logout', 'Admin\\DashboardController@logout')->name('admin.logout');

});

Route::group(['middleware' => ['web', 'validate'], 'prefix' => env('ADMIN_PREFIX')], function($route){

	//utk upload gallery
	Route::post('api/gallery', 'Admin\\Api\\GalleryApi@index');
	Route::post('api/gallery/ckeditor', 'Admin\\Api\\GalleryApi@ckeditor');
});
