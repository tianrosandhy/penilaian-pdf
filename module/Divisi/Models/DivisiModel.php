<?php

namespace Module\Divisi\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class DivisiModel extends Model
{
    //
    protected $table = "cms_divisi";
    protected $fillable = [
    	'title',
    	'image',
        'description',
        'ord',
    	'stat'
    ];

    public function getKaryawan(){
    	return $this->hasMany('Module\Karyawan\Models\KaryawanModel', 'divisi');
    }

}
