<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_kpi_question', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_kpi_group');
            $tb->text('statement');
            $tb->integer('bobot');
            $tb->integer('min_threshold')->default(0);
            $tb->integer('max_threshold')->default(100);
            $tb->integer('ord')->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_kpi_question');
    }
}
