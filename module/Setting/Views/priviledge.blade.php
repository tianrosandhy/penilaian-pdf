@extends('admin.inc.template')
@section('content')


<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
	Tambah Data
</a>
<br>

<hr>

<div class="card">
	<div class="card-block">
		<table class="table data data-table dataTable">
			<thead>
				<tr>
					@foreach($columns as $key=>$val)
						<th>{{ $key }}</th>
					@endforeach
				</tr>
			</thead>
			<thead class="search">
				<tr>
					@foreach($columns as $key=>$val)
						<th>{{ $val['search'] ? $key : "" }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

@include ('admin.create-no-image')
@include ('setting::manage');

@stop

@section('custom-script')

//tweak sompret mengatasi bug HTTP Not Found, padahal ga ada masalah URL	
var tb_data;
setTimeout(function(){
	//initialize datatable
	tb_data = $("table.data-table").DataTable({
		'processing': true,
		'serverSide': true,
		'autoWidth' : false,
		'searching'	: false,
		'filter'	: false,
		'ajax'		: {
			type : 'POST',
			url	: ADMIN_URL + '{{ $datatable_ajax['tb'] }}',
			dataType : 'json',
			data : function(data){
				{!! $datatable_search !!}
			}
		},
		"drawCallback": function(settings) {
			$('[data-init="switchery"]').each(function() {
				var el = $(this);
				new Switchery(el.get(0), {
					size : el.data("size")
				});
			});
		},
	});
},500);
	

	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-form").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-form").attr('action', '');
		$(".row.append").hide();

		form_refresh();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "{{ $datatable_ajax['edit'] }}",
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

				});
				form_refresh();

			}).fail(function(dt){
				unauthorizeModal(dt);
			});

			$(".ajax-form").attr('action', '{{ $edit_action }}/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "{{ $datatable_ajax['delete'] }}",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					unauthorizeModal(dt);
				});

			}, function(){});
		}
	});

	
	//management checkbox section
	$("input.group-click").click(function(){
		var group = $(this).attr('id');
		
		stat = $(this).prop('checked');
		$("input[id^='"+group+"']").prop('checked', stat);
		if(stat == true){
			$("input[id^='"+group+"']").closest('.ctrl-label').addClass('active');
		}
		else{
			$("input[id^='"+group+"']").closest('.ctrl-label').removeClass('active');
		}

	});

	$("body").on("click", '.manage-btn', function(){
		$(".modal .modal-title").html("Manage User Priviledge");
		var mg_id = $(this).attr('data-id');
		$.ajax({
			type : 'POST',
			url : ADMIN_URL+'/setting/priviledge/getpriv/'+mg_id,
			dataType : 'json'
		}).done(function(data){
			$(".ctrl-label").removeClass('active');
			$("#manageForm input[type=checkbox]").each(function(){
				$(this).prop('checked', '');
			});

			$.each(data, function(n){
				$("#"+data[n]).prop('checked', 'checked');
			});

					
			$(".ctrl-label input").each(function(){
				if($(this).is(':checked') == true){
					$(this).closest('.ctrl-label').addClass('active');
				}
			});
		}).fail(function(dt){
			unauthorizeModal(dt);
		});


		$(".ajax-form").attr('action', '{{ $manage_action }}/'+mg_id);	
	});

	$('body').on('change', ".ctrl-label input", function(){
		if($(this).is(':checked')){
			$(this).closest('.ctrl-label').addClass('active');
		}
		else{
			$(this).closest('.ctrl-label').removeClass('active');
		}
	});


@stop