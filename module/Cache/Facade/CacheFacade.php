<?php 

namespace Module\Cache\Facade;

use Illuminate\Support\Facades\Facade;

class CacheFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Cache\\Controllers\\CacheController';
    }
}