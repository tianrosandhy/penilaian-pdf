@foreach($data as $row)
<tr data-id="{{ $row->id }}">
	<td class="drag-handler">
		<i class="fa fa-arrows"></i>
	</td>
	<td>{{ $row->statement }}</td>
	<td>{{ $row->bobot }}</td>
	<td>{{ $row->min_threshold }} - {{ $row->max_threshold }}</td>
	<td>
		@if(hasAccess('admin.kpi.question.update'))
		<a href="#" data-id="{{ $row->id }}" class="btn btn-info edit-btn"><i class="fa fa-pencil"></i></a>
		@endif
	
		@if(hasAccess('admin.kpi.question.destroy'))
		<a href="{{ url('kpi/question/destroy') }}" data-id="{{ $row->id }}" class="btn btn-danger delete-button"><i class="fa fa-trash"></i></a>
		@endif
	</td>
</tr>
@endforeach
