<?php
$router->group(['prefix' => env('ADMIN_PREFIX').'/kpi'], function($route){
	Route::get('/', 'KPIController@index')->name('admin.kpi.data.index');
	Route::post('/', 'KPIController@store')->name('admin.kpi.data.store');
	Route::post('table', 'KPIController@getPost');
	Route::get('switch', 'KPIController@getSwitch');
	Route::get('edit', 'KPIController@edit')->name('admin.kpi.data.edit');
	Route::post('update/{id?}', 'KPIController@update')->name('admin.kpi.data.update');
	Route::get('destroy', 'KPIController@destroy')->name('admin.kpi.data.destroy');



	Route::group(['prefix' => 'question'], function($route){
		$route->get('data/{id?}', 'KPIQuestionController@index')->name('admin.kpi.question.index');
		$route->post('data/{id?}', 'KPIQuestionController@store')->name('admin.kpi.question.store');
		$route->post('data/table/{id}', 'KPIQuestionController@questionData');

		$route->post('edit/{id}', 'KPIQuestionController@edit');
		$route->post('switch', 'KPIQuestionController@getSwitch');
		$route->post('update/{id}', 'KPIQuestionController@update')->name('admin.kpi.question.update');
		$route->post('destroy', 'KPIQuestionController@destroy')->name('admin.kpi.question.destroy');

		$route->post('reorder', 'KPIQuestionController@reorder');
	});



	Route::group(['prefix' => 'scoring'], function($route){
		$route->get('/', 'KPIScoringController@index')->name('admin.kpi.scoring.index');
		$route->post('table', 'KPIScoringController@getPost');
		$route->post('form', 'KPIScoringController@getForm');
		$route->post('form/store', 'KPIScoringController@storeScore')->name('admin.kpi.scoring.store');
	});
});
