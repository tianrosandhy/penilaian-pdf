<?php
namespace Module\KPI\Models;

use Illuminate\Database\Eloquent\Model;
use Request;

class KPIDivisi extends Model
{
    //
    protected $table = "cms_kpi_to_divisi";
    protected $fillable = [
    	'id_kpi',
    	'id_divisi',
    	'stat'
    ];

    
    public function group(){
    	return $this->belongsTo('Module\KPI\Models\KPIModel', 'id_kpi');
    }

    public function divisi(){
    	return $this->belongsTo('Module\Divisi\Models\DivisiModel', 'id_divisi');
    }

}
