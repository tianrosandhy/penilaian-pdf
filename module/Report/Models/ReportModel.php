<?php
namespace Module\Report\Models;

use Illuminate\Database\Eloquent\Model;
use Request;

class ReportModel extends Model
{
    //
    protected $table = "cms_report";
    protected $fillable = [
    	'id_karyawan',
    	'id_divisi',
    	'id_penilai',
    	'periode',
    	'skor',
        'bobot',
        'index',
        'kpi',
    	'stat'
    ];

    public function karyawan(){
    	return $this->belongsTo('Module\Karyawan\Models\KaryawanModel', 'id_karyawan');
    }

    public function divisi(){
    	return $this->belongsTo('Module\Divisi\Models\DivisiModel', 'id_divisi');
    }

    public function penilai(){
    	return $this->belongsTo('Module\Setting\Models\UserModel', 'id_penilai');
    }

    
}
