@extends ('admin.inc.template')

@push ('style')
	{!! register_single('assets/plugins/bootstrap-datepicker/css/datepicker.css', 'css') !!}
@endpush

@section ('content')
<h2>{{ $title }}</h2>
<div class="card">
	<div class="card-block">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Periode</label>
					<input type="text" name="periode" value="{{ $periode }}" class="periode-timepicker form-control">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label>Divisi</label>
					<select name="divisi" class="form-control dvsi-cbox">
						<option value="">- Seluruh Divisi -</option>
						@foreach($divisi as $dv => $dvname)
						<option value="{{ $dv }}" {{ $selected['divisi'] == $dv ? 'selected' : '' }}>{{ $dvname }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>		
		<center>
			<a href="{{ Request::fullUrl() }}&strict=1" class="btn btn-primary refresh-button">Refresh Report</a>
		</center>
	</div>
</div>



<table class="data table">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Jabatan</th>
			<th>Penilai</th>
			<th>Skor</th>
			<th>Index</th>
			<th>Final KPI</th>
		</tr>
	</thead>
	<tbody>
		{!! $tableData !!}
	</tbody>
</table>

<center>
	<a href="{{ admin_url('report/print') }}?{{ Request::getQueryString() }}" target="_blank" class="btn btn-info">
		<i class="fa fa-print"></i>
		Print
	</a>
</center>


@stop

@push('script')
{!! register_single('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', 'js') !!}
<script>
$(function(){
	$(".periode-timepicker").datepicker({
	    format: "MM yyyy",
	    startView: 2,
	    minViewMode: 1
	});

	$(".periode-timepicker, .dvsi-cbox").on('change', function(){
		per = $('.periode-timepicker').val();
		dv = $('.dvsi-cbox').val();
		location.href = window.location.origin + window.location.pathname + '?periode=' + per + '&divisi=' + dv;
	});


	$(".refresh-button").on('click', function(e){
		e.preventDefault();
		per = $('.periode-timepicker').val();
		dv = $('.dvsi-cbox').val();
		location.href = window.location.origin + window.location.pathname + '?periode=' + per + '&divisi=' + dv + '&strict=1';
	});

});	
</script>
@endpush