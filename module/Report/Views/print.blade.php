<div align="center">
	<h1>{{ isset($title) ? $title : '' }}</h1>
	<h3>{{ isset($subtitle) ? $subtitle : '' }}</h3>
</div>
<hr>
<table>
	<tr>
		<td>Periode</td>
		<td> : </td>
		<td>{{ $periode }}</td>
	</tr>
	<tr>
		<td>Divisi</td>
		<td> : </td>
		<td>{!! $dvname !!}</td>
	</tr>
</table>

<table class="data table" style="width:100%; margin-top:10px;" border=1 cellspacing=0 cellpadding=5>
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Jabatan</th>
			<th>Penilai</th>
			<th>Skor</th>
			<th>Index</th>
			<th>Final KPI</th>
		</tr>
	</thead>
	<tbody>
		{!! $data !!}
	</tbody>
</table>
