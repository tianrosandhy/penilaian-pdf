<?php
namespace Module\Setting\Traits;

use Form;
use App\Model\DataTableModel;
use Request;
use Route;

use Module\Setting\Models\UserPriviledgeModel;

use App\Http\Controllers\Admin\Api\DatatableApi;

trait UserPriviledgeTrait
{

	//Structure Trait

	public function register_column(){
		$this->columns = [
			'Priviledge Name' => ['search' => true, 'col' => 'priviledge_name'],
			'' => ['search' => false, 'col' => 'button'], 
		];
	}

    //
    public function register_form(){
		$this->forms = [
			'Priviledge Name' => Form::input('text', 'priviledge_name', old('priviledge_name', null), ['class' => 'form-control'])
		];    	
    }



    //Data Table Trait

    public function getPost(){
    	$request = $this->request->all();

    	$dt = new DatatableApi($request, new UserPriviledgeModel(), $this->columns);
    	$output = $dt->generate(true);
    	$output['data'] = $this->table_format($request, $output);
    	//hapus query result mentah
    	unset($output['result']);

    	return json_encode($output);
    }

    public function table_format($data, $output){
    	if(count($output['result']) == 0){
			$ret = $output['data'];
		}
		else{
			foreach($output['result'] as $row){
				//field index disesuaikan dengan parameter col di this->column
				$ret[] = array(
					$row->priviledge_name,
					'<div class="btn-group">
						<a data-id="'.$row->id.'" class="btn-info btn btn-sm waves-effect manage-btn" data-toggle="modal" data-target="#manageForm">Manage Permission</a>
						<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>
						<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>
					</div>'
				);
			}
		}
		return $ret;
	}


	public function get_user_priviledge($id){
		$data = $this->model->find($id);
		if(strlen($data->permission) == 0)
			return [];

		$permission = json_decode($data->permission, true);

		//sebelum lupa, jangan lupa tanda titik dibalikin ke underscore lagi
		$imp = [];
		foreach ($permission as $value) {
			$imp[] = str_replace(".", "_", $value);
		}		

		return json_encode($imp);
	}	

	public function get_priviledge_list($except=['dashboard','login','postlogin','api']){
		//FUCKING METHOD TO STRUCTURIZED PRIVILEDGE IN ROUTE LISTS

		$routeCollection = Route::getRoutes();

		$save = [];
		foreach ($routeCollection as $value) {
			$routename = $value->getName();
			if(strlen($routename) > 0){
				$x[] = explode(".", $routename);
			}

		}

		$a = array_unique(array_column($x, 1));
		$a = array_flip($a);
		foreach($a as $k=>$aa){
			$a[$k] = [];
		}


		foreach($x as $row){
			if(array_key_exists($row[1], $a)){
				if(isset($row[2])){
					$a[$row[1]][$row[2]] = [];
					if(isset($row[3])){
						$baru[$row[1]][$row[2]][$row[3]] = [];
					}
				}
			}

		}


		$last = array_merge($a, $baru);
		foreach($last as $key=>$module){
			if(in_array($key, $except)){
				unset($last[$key]);
			}
		}

		//jadi, seluruh route list ada di $last
		return $last;
	}

}