<!DOCTYPE html>
<html>
  <head>
    @include ('admin.inc.partials.metadata')
  </head>
  <body class="fixed-header menu-pin">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <!-- LOGO -->
        @if(strlen(Setting::getParam('logo')) == 0)
        <img src="{!! assets('assets/img/logo.png') !!}" alt="logo" width="78" height="22">
        @else
        <img src="{!! url('upload/'.Setting::getParam('logo')) !!}" alt="logo" height=50>
        @endif
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          @foreach(config('admin.menu') as $label => $menu )
            @if(Setting::adminCheckPermission($menu['target'], (isset($menu['submenu']) ? $menu['submenu'] : []) ))
            <li class="{{ Request::route()->getName() == $menu['target'] ? 'active' : '' }} {{ $menu['target'] === null ? 'parent' : '' }}">
              <a href="{{ $menu['target'] === null ? 'javascript:;' : url(route($menu['target'])) }}">
                  <span class="title">{{ $label }}</span>

              @if(isset($menu['submenu']))
              <span class=" arrow"></span>
              @endif
              </a>
              <span class="icon-thumbnail"><i class="fa {{ $menu['icon'] }}"></i></span>

              @if(isset($menu['submenu']))
              <ul class="sub-menu">
                @foreach($menu['submenu'] as $sublbl => $target)
                  @if(Setting::adminCheckPermission($target))
                  <li {!! $target == Request::route()->getName() ? 'class="active"' : "" !!}>
                    <a href="{{ url(route($target)) }}">{{ $sublbl }}</a>
                    <span class="icon-thumbnail">c</span>
                  </li>
                  @endif
                @endforeach
              </ul>
              @endif
            </li>
            @endif
          @endforeach

        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
        </a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
          <div class="brand inline">
            <img src="{{ assets('assets/img/logo.png') }}" alt="logo"  width="78" height="22">
          </div>
          
        </div>
        <div class="d-flex align-items-center">
          <!-- START User Info-->
          <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
            <span class="semi-bold">{{ Auth::user()['username'] }}</span>
          </div>
          <div class="dropdown pull-right hidden-md-down">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="thumbnail-wrapper d32 circular inline">
              <img src="{{ assets('assets/img/profiles/avatar.jpg') }}" alt="" width="32" height="32">
              </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
              <a href="{{ url(env('ADMIN_PREFIX').'/'.'setting') }}" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
              <a href="{{ url(env('ADMIN_PREFIX').'/'.'logout') }}" class="clearfix bg-master-lighter dropdown-item">
                <span class="pull-left">Logout</span>
                <span class="pull-right"><i class="pg-power"></i></span>
              </a>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->





      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class=" container-fluid   container-fixed-lg sm-p-l-0 sm-p-r-0">
              <div class="inner">
                <!-- START BREADCRUMB -->
                @include ('admin.inc.partials.breadcrumb')
                <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->

            @yield('content')

            <!-- END PLACE PAGE CONTENT HERE -->
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid  container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">{!! Setting::getParam('copyright') !!}</span>
              <span class="hint-text">All rights reserved. </span>
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
              Hand-crafted <span class="hint-text">&amp; made with Love</span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    
    @include ('admin.inc.partials.foot')
    {!! Setting::getParam('custom_script') !!}
  </body>
</html>