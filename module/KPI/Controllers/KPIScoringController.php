<?php
namespace Module\KPI\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Module\KPI\Traits\KPIScoringTrait;

class KPIScoringController extends Controller
{
	use KPIScoringTrait;

	public $request;

	public function __construct(Request $req){
		$this->request = $req;
	}

	public function index(){
		$periode = date('F Y');
		if(isset($this->request->periode)){
			$periode = date('F Y', strtotime($this->request->periode));
		}

		$title = 'KPI Scoring';
		return view('kpi::scoring', compact(
			'periode',
			'title'
		));
	}

}