<?php

namespace Module\Setting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use App\Model\DataTableModel;
use Form;
use Validator;
use Auth;

use Module\Setting\Models\UserModel;
use Module\Setting\Models\UserPriviledgeModel;
use Module\Setting\Traits\UserPriviledgeTrait;


class SettingPriviledgeController extends Controller
{
	public 	$request, 
			$response,
			$columns,
			$forms,
			$model;

	use UserPriviledgeTrait;
	
    //
	public function __construct(Request $req, Response $red){
		$this->request = $req;
		$this->response = $red;
		$this->model = new UserPriviledgeModel();

		$this->register_column();
	}

    public function index(){
    	$this->register_form();
    	

    	return view('setting::priviledge')->with([
    		'title' => 'Priviledge Group',
    		'columns' => $this->columns,
    		'forms' => $this->forms,
    		'priviledge_list' => $this->get_priviledge_list(),
    		'datatable_search' => DataTableModel::generate_search($this->columns),
    		'datatable_ajax' => [
    			'tb' => '/setting/priviledge/table',
    			'switch' => '/setting/priviledge/switch',
    			'edit' => '/setting/priviledge/edit',
    			'delete' => '/setting/priviledge/destroy'
    		],
    		'manage_action' => route('admin.setting.priviledge.manage'),
    		'edit_action' => route('admin.setting.priviledge.update'),
    		'additional_css' => config('asset.group.index')['css'],
    		'additional_js' => config('asset.group.index')['js'],
    	]);
    }

    public function store(){
    	$rule = [
    		'priviledge_name' => 'required|max:80',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('priviledge_name', $this->request->priviledge_name)
			->get();

		if(count($cek) > 0){
			$out['error'][][0] = "Group priviledge dengan nama tersebut sudah digunakan";
		}
		else{
			//proses simpan
			$this->model->insert([
				'priviledge_name' => $this->request->priviledge_name,
				'type' => 1,
				'stat' => 1
			]);
			$out['success'] = "Berhasil menyimpan group priviledge baru";
		}

    	return json_encode($out);

    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
    		'priviledge_name' => 'required|max:80',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('priviledge_name', $this->request->priviledge_name)
			->where('id','<>',$id)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data group permission dengan nama tersebut sudah ada.";
		}
		else{
			$data = $this->model->find($id);
			if($data->type == 0){
				$out['error'][][0] = "Mohon maaf, grup user Admin utama tidak dapat diupdate";
			}
			else{
				$data->priviledge_name = $this->request->priviledge_name;
				$data->save();
				$out['success'] = "Berhasil mengupdate priviledge name";
			}

		}

    	return json_encode($out);
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek->type > 0){
		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Tidak dapat menghapus group priviledge admin utama";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    public function manage($id){
    	$post = $this->request->all();
    	unset($post['_token']);

    	$saved = [];
    	foreach($post as $routename => $val){
    		$saved[] = str_replace("_", ".", $routename);
    	}

    	$saved = json_encode($saved);
    	$current_user = $id;

    	$user = $this->model->find($current_user);
    	$user->permission = $saved;
    	$user->save();

    	return json_encode(['success' => 'Berhasil mengatur group permission priviledge']);
    }
}
