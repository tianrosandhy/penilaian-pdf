<?php

namespace App\Http\Controllers\Admin\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


//Controller publik utk pengolahan query datatable
class DatatableApi extends Controller
{
	public 
		$model,
		$columns, // column information
		$data; //request data

    //
	public function __construct($req, $model, $columns){
		$this->data = $req;
		$this->model = $model;
		$this->search = [];
		$this->columns = $columns;
	}



	public function generate($withstat = true, $custom_where = [], $custom_order = []){

		$where_data = self::where_building();
		if($custom_where){
			$where_data = array_merge($where_data, [$custom_where]);
		}

		$result = self::query_result($withstat, $where_data);
		$out['draw'] = $this->data['draw'];

		$out['recordsTotal'] = count($result);
		$out['recordsFiltered'] = count($result);


		$filtered = self::query_result($withstat, $where_data, true, $custom_order);
		$out['result'] = $filtered;
		$out['data'] = self::blank_action();

		return $out;
	}





	protected function blank_action(){
		$rt = [];
		for($i=0; $i<count($this->data['columns']); $i++){
			array_push($rt, '');
		}
		$ret[] = $rt;
		return $ret;
	}

	protected function where_building(){
		$where_data = []; 
		foreach($this->columns as $col){
			if(isset($col['search'])){
				if($col['search'] == true){
					if(isset($this->data[$col['col']])){
						$where_data[] = [$col['col'], '%'.$this->data[$col['col']].'%'];
					}
				}
			}
		}
		return $where_data;
	}



	protected function query_result($withstat=true, $where_data = [], $limit=false, $custom_order = []){
		$result = $this->model;

		//if there is control stat column
		if($withstat){
			$result = $result->where('stat', '<>', 9);
		}

		//filter by search query
		if(count($where_data) > 0){
			foreach($where_data	as $where){
				if(count($where) == 2){
					$result = $result->where($where[0], 'like' , $where[1]);
				}
				else if(count($where) == 3){
					$result = $result->where($where[0], $where[1], $where[2]);
				}
			}
		}

		//get orderby
		$index = $this->data['order'][0]['column'];
		$arah = $this->data['order'][0]['dir'];

		//just limit for query result
		if($limit){
			$result = $result->skip($this->data['start'])
				->take($this->data['length']);
		}

		$kolom = array_values($this->columns);

		if(isset($kolom[$index])){
			$result = $result->orderBy($kolom[$index]['col'], $arah);
		}
		if(count($custom_order) > 0){
			$result = $result->orderBy($custom_order[0], $custom_order[1]);
		}

		return $result->get();
	}
}
