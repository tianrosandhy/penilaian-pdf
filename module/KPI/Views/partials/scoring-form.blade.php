@foreach($data as $kpi)
	@if(count($kpi->group) > 0)
	<form action="{{ url('kpi/scoring/form/store') }}" method="post" class="ajax-form">
		<div class="card card-default">
			<div class="card-header separator">
				<div class="card-title">{{ $kpi->group->title }}</div>
			</div>
			<div class="card-block">
				<input type="hidden" name="id_karyawan" value="{{ $id_karyawan }}">
				<input type="hidden" name="periode" value="{{ $periode }}">
				@foreach($kpi->group->question as $question)
				<?php
				$value = 0;
				if(!empty($filledData)){
					if($filledData->has($question->id)){
						$value = $filledData[$question->id];
					}
				}
				?>
				<div class="card">
					<div class="card-block">
						<div class="form-group">
							<label>
								{{ $question->statement }}
							</label>
							<div>
								<input name="score[{{ $question->id }}]" 
									type="range" 
									min="{{ $question->min_threshold }}"
									max="{{ $question->max_threshold }}"
									step="1"
									data-bobot="{{ $question->bobot }}"
									value="{{ $value }}" 
									data-orientation="horizontal"
									class="form-control">
							</div>
						</div>
					</div>
				</div>
				@endforeach

				<center>
					<button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				</center>
			</div>
		</div>
	</form>
	@endif
@endforeach