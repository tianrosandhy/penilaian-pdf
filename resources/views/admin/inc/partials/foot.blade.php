@yield('modal')

{!! Cache::merge([
  'assets/plugins/pace/pace.min.js',
  'assets/plugins/modernizr.custom.js',
  'assets/plugins/jquery-ui/jquery-ui.min.js',
  'assets/plugins/tether/js/tether.min.js',
  'assets/plugins/bootstrap/js/bootstrap.min.js',
  'assets/js/popper.min.js',
  'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
  'assets/plugins/select2/js/select2.full.min.js',
  'assets/plugins/switchery/js/switchery.min.js',
  'assets/plugins/jquery-nestable/jquery.nestable.min.js',
  'assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js',
  'assets/plugins/typeahead/typeahead.bundle.js',
  
  'custom/jquery-debounce.js',
  'assets/js/sweetalert.min.js',
  'assets/plugins/alerts/alertify.min.js',
  'assets/plugins/summernote/summernote-bs4.min.js',
  'assets/plugins/dropzone/dropzone.min.js',
  'assets/js/bootstrap-tagsinput.min.js',
], [
  'dir' => 'admin_theme/',
  'type' => 'js'
]) !!}

<script src="https://cdn.ckeditor.com/4.7.3/standard-all/ckeditor.js"></script>

{!! register_single('assets/plugins/jquery-datatable/jquery.dataTables.js', 'js') !!}
{!! register_single('assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js', 'js') !!}
{!! register_single('assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js', 'js') !!}
{!! register_single('assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js', 'js') !!}
{!! register_single('assets/plugins/jquery-datatable/extensions/export/jszip.min.js', 'js') !!}
{!! register_single('assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js', 'js') !!}

{!! register_single('assets/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js', 'js') !!}
{!! register_single('assets/plugins/typeahead/typeahead.bundle.js', 'js') !!}

{!! register_single('pages/js/pages.min.js', 'js') !!}
{!! register_single('assets/js/scripts.js', 'js') !!}
@include('admin.inc.script')
@stack ('scripts')
@stack ('script')