@push ('style')
{!! register_single('assets/plugins/ion-slider/css/ion.rangeSlider.css', 'css') !!}
{!! register_single('assets/plugins/ion-slider/css/ion.rangeSlider.skinModern.css', 'css') !!}
@endpush
<div id="scoringModal" class="modal fade fill-in">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<h5>
					<span class="bold">KPI Scoring</span> Karyawan
				</h5>
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-sm-6">
						<div class="card">
							<div class="card-block">
								<div class="form-group">
									<label>Nama Karyawan</label>
									<div class="name-holder">...</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-block">
								<div class="form-group">
									<label>Periode</label>
									<div class="periode-holder">...</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="card">
							<div class="card-block">
								<div class="form-group">
									<label>Divisi</label>
									<div class="divisi-holder">...</div>
								</div>			
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card">
							<div class="card-block">
								<div class="form-group">
									<label>Jabatan</label>
									<div class="jabatan-holder">...</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<h5 class="font-montserrat">Masukkan skor penilaian Anda untuk karyawan diatas</h5>
				<div class="score-container">
					
				</div>

			</div>
		</div>
	</div>
</div>

@push ('script')
{!! register_single('assets/plugins/ion-slider/js/ion.rangeSlider.min.js', 'js') !!}
<script>
$(function(){

	$("body").on('click', '.btn-scoring', function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		periode = $(".periode-timepicker").val();
		$.ajax({
			url : BASE_URL + '/kpi/scoring/form',
			type : 'POST',
			dataType : 'json',
			data : {
				id : id,
				periode : periode
			},
			success : function(resp){
				$("#scoringModal").modal('show');

				$(".name-holder").html(resp.karyawan.title);
				$(".divisi-holder").html(resp.karyawan.get_divisi.title);
				$(".jabatan-holder").html(resp.karyawan.jabatan);
				$(".periode-holder").html($(".periode-timepicker").val());

				$(".score-container").html(resp.form);
				$("input[type='range']").each(function(){
					$(this).ionRangeSlider({
						min : $(this).attr('min'),
						max : $(this).attr('max'),
						from : $(this).attr('value'),
						grid : true,
					});
				});

			},
			error : function(resp){
				sweet_alert('error', 'Server Error. Sorry, we cannot process your last request.');
			}
		});
	});


	$("body").on('submit', '.ajax-form', function(e){
		e.preventDefault();
		data = $(this).serialize();
		$.ajax({
			url : $(this).attr('action'),
			type : $(this).attr('method'),
			dataType : 'json',
			data : data,
			success : function(resp){
				sweet_alert(resp.type, resp.message);
				if(resp.type == 'success'){
					tb_data.ajax.reload();
					$("#scoringModal").modal('hide');
				}
			}

		});
	});
});
</script>
@endpush