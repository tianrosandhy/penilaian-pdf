<?php
$width = 0;
if($skorData['bobot'] > 0){
	$width = round(($skorData['skor'] / $skorData['bobot']) * 100);
}
?>
{{ $skorData['skor'] }} / {{ $skorData['bobot'] }}
<br>
<div class="progress">
	<div class="progress-bar progress-bar-complete" style="width:{{ $width }}%"></div>
</div>

@foreach($skorData as $username => $data)
	@if(isset($data['_bobot']) && isset($data['_skor']))
	<div>
		<strong>{{ $username }}</strong>
		<span>
			{{ $data['_skor'] }} / {{ $data['_bobot'] }}
		</span>
	</div>
	@endif

@endforeach