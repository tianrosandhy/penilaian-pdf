@extends('admin.inc.template')

@section ('content')

<div class="row">
	<div class="col-sm-6">
		<select name="kpi_group" id="kpi_group" class="form-control">
			<option value="">- Select KPI Group -</option>
			@foreach($groupList as $kpi)
			<option value="{{ $kpi->id }}" {{ $idkpi == $kpi->id ? 'selected' : '' }}>{{ $kpi->title }}</option>
			@endforeach
		</select>		
	</div>
	<div class="col-sm-6">
		@if(hasAccess('admin.kpi.question.store'))
			@if($idkpi > 0)
				<a class="btn btn-primary" id="add-btn" data-toggle="modal" data-target="#modalForm"><i class="fa fa-plus"></i> Add Question Data</a>
			@endif
		@endif
	</div>
</div>


@if($idkpi > 0)
<table class="table data" id="question-table">
	<thead>
		<tr>
			<th>#</th>
			<th>Statement</th>
			<th>Bobot</th>
			<th>Threshold</th>
			<th></th>
		</tr>
	</thead>
	<tbody class="tb-append" id="sortable-table">
		{!! $questionData !!}
	</tbody>
</table>

<input type="hidden" name="orderData" value="{{ json_encode($order_data) }}">
<div class="save-reorder padd" style="display:none">
	<a href="#" class="btn btn-sm btn-info" id="store-reorder"><i class="fa fa-save"></i> Reorder Questions</a>
</div>

@endif


@include ('kpi::question-modal')

@stop


@push ('scripts')
{!! register_single('assets/plugins/sortable/Sortable.js', 'js') !!}
<script>
$(function(){

	$("#kpi_group").on('change', function(){
		val = $(this).val();
		location.href = BASE_URL + '/kpi/question/data/' + val;
	});

	Sortable.create(
	    $('#sortable-table')[0],
	    {
	        animation: 150,
	        scroll: true,
	        handle: '.drag-handler',
	        onEnd : function(){
	        	rowOrdering();
	        }
	    }
	);



	$('#add-btn').on('click', function(){
		$("h3.modal-title").html('Insert KPI Question Data');
		$(".custom-ajax-form")[0].reset();
		$(".custom-ajax-form").removeAttr('action');
	});

	$("body").on('click', '.edit-btn', function(e){
		e.preventDefault();
		id = $(this).attr('data-id');
		$.ajax({
			url : BASE_URL + '/kpi/question/edit/'+id,
			dataType : 'json',
			type : 'POST',
			success : function(resp){
				$("h3.modal-title").html('Update KPI Question Data');
				$(".custom-ajax-form").attr('action', BASE_URL + '/kpi/question/update/'+id);
				$(".modal").modal('show');
				$('[name="statement"]').val(resp.statement);
				$('[name="bobot"]').val(resp.bobot);
				$('[name="min_threshold"]').val(resp.min_threshold);
				$('[name="max_threshold"]').val(resp.max_threshold);
			},
			error : function(){
				sweet_alert('error', 'Sorry, we cannot process your last request');
			}
		});
	});



	$("body").on('click', '.delete-button', function(e){
		e.preventDefault();
		post_id = $(this).attr('data-id');
		target = $(this).attr('href');
		alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				msghandling(dt);
				refreshTable();
			});

		}, function(){});
	});



	$("#store-reorder").on('click', function(e){
		e.preventDefault();
		orderData = $('[name="orderData"]').val();
		$.ajax({
			url : BASE_URL + '/kpi/question/reorder',
			type : 'POST',
			dataType : 'json',
			data : {
				orderData : orderData,
				group : $("#kpi_group").val()
			},
			success : function(resp){
				if(resp.type == 'success'){
					$(".save-reorder").slideUp();
				}
				sweet_alert(resp.type, resp.message);
			}
		});
	});

});

function rowOrdering(){
	var obj = new Array;
	$("table.data tr").each(function(){
		if($(this).attr('data-id') !== undefined){
			obj.push($(this).attr('data-id'));
		}
	});

	$("input[name=orderData]").val(JSON.stringify(obj));

	$(".save-reorder").slideDown();
}


function refreshTable(){
	val = $("#kpi_group").val();
	$.ajax({
		url : BASE_URL + '/kpi/question/data/table/'+val,
		type : "POST",
		dataType : 'html',
		success : function(resp){
			$(".tb-append").html(resp);
		}
	});
}
</script>
@endpush