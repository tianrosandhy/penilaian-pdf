<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Karyawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_karyawan', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->string('slug',190);
            $tb->string('tempat')->nullable();
            $tb->date('tgl_lahir')->nullable();
            $tb->tinyinteger('jk')->nullable();
            $tb->string('email')->nullable();
            $tb->string('telp')->nullable();
            $tb->string('hp')->nullable();
            $tb->string('image',190)->nullable();
            $tb->string('description')->nullable();
            $tb->integer('divisi')->nullable();
            $tb->string('jabatan')->nullable();
            $tb->integer('ord');
            $tb->timestamps();            
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cms_karyawan');
    }
}
