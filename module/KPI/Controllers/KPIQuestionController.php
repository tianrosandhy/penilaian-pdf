<?php
namespace Module\KPI\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Module\KPI\Models\KPIQuestion;
use Module\KPI\Models\KPIModel;
use Module\KPI\Traits\KPIQuestionTrait;

class KPIQuestionController extends Controller
{
	use KPIQuestionTrait;

	public $request;

	public function __construct(Request $req){
		$this->request = $req;
		$this->model = new KPIQuestion();
	}

	public function index($idkpi=0){
		$title = 'Question Data';
		$groupList = self::kpiGroupList();

		$groupkpi = $order_data = $groupList->pluck('id');
		if($idkpi > 0 && !$groupkpi->contains($idkpi)){
			abort(404);
		}

		$questionData = self::questionData($idkpi);


		return view('kpi::question', compact(
			'title',
			'groupList',
			'idkpi',
			'questionData',
			'order_data'
		));
	}

	public function store($idkpi=0){
		$validation = self::validation();
		if($validation->fails()){
			return ajax_response($validation->messages()->first());
		}

		$groupList = self::kpiGroupList();
		$groupKpi = $groupList->pluck('id');
		if($idkpi == 0 || !$groupKpi->contains($idkpi)){
			abort(404);
		}

		//store logic
		$saveData = [
			'id_kpi_group' => $idkpi,
			'statement' => $this->request->statement,
			'bobot' => $this->request->bobot,
			'stat' => 1
		];

		if($this->request->min_threshold > 0){
			$saveData['min_threshold'] = $this->request->min_threshold;
		}
		if($this->request->max_threshold > 0){
			$saveData['max_threshold'] = $this->request->max_threshold;
		}
		KPIQuestion::create($saveData);

		return ajax_response('Question KPI Data has been saved', 'success');
	}

	public function edit($id){
		$data = KPIQuestion::findOrFail($id);
		return $data;
	}

	public function update($id){
		$validation = self::validation();
		if($validation->fails()){
			return ajax_response($validation->messages()->first());
		}

		$instance = KPIQuestion::findOrFail($id);

		//store logic
		$saveData = [
			'statement' => $this->request->statement,
			'bobot' => $this->request->bobot,
			'min_threshold' => $this->request->min_threshold,
			'max_threshold' => $this->request->max_threshold,
		];

		$instance->update($saveData);

		return ajax_response('Question KPI Data has been updated', 'success');
	}

	public function destroy(){
		if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}

	}




	public function reorder(){
		$validation = \Validator::make($this->request->all(), [
			'orderData' => 'required',
			'group' => 'required'
		]);
		if($validation->fails()){
			return ajax_response($validate->messages()->first());
		}

		$orderData = json_decode($this->request->orderData);
		$n = 0;
		foreach($orderData as $ord){
			$inst = KPIQuestion::find($ord);
			$inst->ord = $n;
			$inst->save();
			$n++;
		}

		return ajax_response('Berhasil mengurutkan data question', 'success');
	}
}