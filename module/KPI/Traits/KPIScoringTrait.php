<?php
namespace Module\KPI\Traits;

use Module\KPI\Models\KPIModel;
use Module\KPI\Models\KPIDivisi;
use Module\KPI\Models\KPIQuestion;
use Module\KPI\Models\KPIScoring;
use Module\Karyawan\Models\KaryawanModel;
use Module\Setting\Models\UserModel;

trait KPIScoringTrait
{

	public $field = [
		0 => 'title',
		1 => 'divisi',
		2 => 'Jabatan'
	];

	public function getPost(){
		
		$orderBy = $this->field[0];
		$orderWay = $this->request->order[0]['dir'];
		if(isset($this->field[$this->request->order[0]['column']])){
			$orderBy = $this->field[$this->request->order[0]['column']];
		}

		$dataUser = self::getUserKPI($this->request->periode, $orderBy, $orderWay);

		$data = self::tableView($dataUser);

		return [
			'data' => $data,
			'recordsFiltered' => count($data),
			'recordsTotal' => count($data)
		];

	}

	protected function getUserKPI($periode, $order, $way){
		$periode = date('Y-m-d', strtotime($periode));

		$data = KaryawanModel::with([
			'getDivisi',
			'getScore' => function($qry) use($periode){
				$qry->where('periode', $periode);
			}
		])
		->where('stat', 1)
		->orderBy($order, $way)
		->get();

		return $data;
	}

	protected function tableView($data){
		$periode = date('Y-m-d', strtotime($this->request->periode));

		$out = [];
		foreach($data as $row){
			$skorData = self::getScoreData($row->getScore);
			$btn = '';
			if(current_user()->getPriviledge->priviledge_name == 'Penilai' || current_user()->priviledge == 1){ //diupdate biar superadmin juga bole ngasi skoring
				$btn = '<a href="#" data-id="'.$row->id.'" class="btn btn-primary btn-scoring">'.
					(count($row->getScore) == 0 ? 
					'Create Score Data' : 
					'Update Score Data').'</a>';
			}
			
			$out[] = [
				$row->title,
				$row->getDivisi->title,
				$row->jabatan,
				view('kpi::partials.score-table', compact('skorData'))->render(),
				$btn
			];
		}
		if(count($data) == 0){
			$out = ['', '', '', '', ''];
		}
		
		return $out;
	}


	protected function getScoreData($instance){
		$truth = [];
		$userData = [];
		foreach($instance as $row){
			$min = $row->question->min_threshold;
			$max = $row->question->max_threshold;
			$bobot = $row->question->bobot;
			$threshold = $max - $min;
			$real_value = $row->value - $min;
			$percentage = $real_value / $threshold;


			$userData[$row->id][$row->id_penilai]['value'] = $row->value;
			$userData[$row->id][$row->id_penilai]['realValue'] = $real_value;
			$userData[$row->id][$row->id_penilai]['percentage'] = $percentage;
			$userData[$row->id][$row->id_penilai]['bobot'] = $bobot;
			$userData[$row->id][$row->id_penilai]['skor'] = $percentage * $bobot;

		}

		//hasil pengolahan
		$final = [
			'bobot' => 0,
			'skor' => 0
		];


		$listUser = UserModel::where('priviledge', '>', 0)
			->get()
			->pluck('username', 'id')
			->toArray();

		foreach($userData as $data){
			foreach($data as $id_penilai => $user){
				$final['bobot'] += $user['bobot'];
				$final['skor'] += $user['skor'];

				if(isset($listUser[$id_penilai])){
					$username = $listUser[$id_penilai];
					if(isset($final[$username]['_bobot'])){
						$final[$username]['_bobot'] += $user['bobot'];
					}
					else{
						$final[$username]['_bobot'] = $user['bobot'];
					}
					if(isset($final[$username]['_skor'])){
						$final[$username]['_skor'] += $user['skor'];
					}
					else{
						$final[$username]['_skor'] = $user['skor'];
					}
				}

			}

		}

		return $final;

	}




	public function getForm(){
		$periode = date('Y-m-d', strtotime($this->request->periode));
		if(empty($this->request->periode)){
			$periode = date('Y-m').'-01';
		}

		$cek = KaryawanModel::with([
			'getDivisi',
			'getScore' => function($qry) use ($periode){
				$qry->where('periode', $periode);
			}
		])->findOrFail($this->request->id);

		$form = self::getFormByDivisi($this->request->id, $cek->divisi, $periode);

		return [
			'karyawan' => $cek,
			'form' => $form
		];
	}


	protected function getFormByDivisi($id_karyawan, $id_divisi, $periode){
		$periode = date('Y-m-d', strtotime($periode));
		$data = KPIDivisi::with([
				'group' => function($qry){
					$qry->whereHas('question', function($q){
						$q->orderBy('ord', 'ASC');
					});
				}
			])
			->where('id_divisi', $id_divisi)
			->get();


		//filled data
		$filledData = KPIScoring::where([
			'id_user' => $id_karyawan,
			'periode' => $periode,
			'id_penilai' => \Auth::user()->id
		])->get();
		if(count($filledData) > 0){
			$filledData = $filledData->pluck('value', 'id_question');
		}
		else{
			$filledData = null;
		}

		return view('kpi::partials.scoring-form', compact('data', 'periode', 'id_karyawan', 'filledData'))->render();
	}


	public function storeScore(){
		$validate = \Validator::make($this->request->all(), [
			'id_karyawan' => 'required|integer',
			'periode' => 'required|date',
			'score' => 'required|array'
		]);

		$id_penilai = current_user()->id;

		if($validate->fails()){
			return ajax_response($validate->messages()->first());
		}

		//proses penyimpanan
		$periode = date('Y-m-d', strtotime($this->request->periode)); #biar formatnya sama aja
		//hapus data scoring lama jika ada
		KPIScoring::where([
			'id_user' => $this->request->id_karyawan,
			'id_penilai' => $id_penilai,
			'periode' => $periode
		])->delete();

		foreach($this->request->score as $id_question => $value){
			KPIScoring::create([
				'id_user' => $this->request->id_karyawan,
				'periode' => $periode,
				'id_question' => $id_question,
				'id_penilai' => $id_penilai,
				'value' => $value,
				'stat' => 1
			]);
		}

		return ajax_response('Berhasil menyimpan data skor KPI karyawan', 'success');

	}





}