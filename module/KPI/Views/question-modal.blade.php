<div id="modalForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			{{ Form::open(['method' => 'POST', 'class' => 'custom-ajax-form']) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Tambah Data</h3>
			</div>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Statement</label>
							<textarea name="statement" class="form-control"></textarea>
						</div>
						
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label>Bobot</label>
							<input type="number" class="form-control" name="bobot" min=0>
						</div>

						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label>Minimum Threshold</label>
									<input type="number" class="form-control" name="min_threshold" placeholder="0">
								</div>
								
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label>Maximum Threshold</label>
									<input type="number" class="form-control" name="max_threshold" placeholder="100">
								</div>
								
							</div>
						</div>
						
						
					</div>
				</div>

								



			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">
					<span class="fa fa-save"></span>
					Simpan
				</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@push ('scripts')
<script>
$(function(){
	$(".custom-ajax-form").on('submit', function(e){
		e.preventDefault();
		data = $(this).serialize();

		action = null;
		if($(this).attr('action') !== undefined){
			action = $(this).attr('action');
		}

		$.ajax({
			url : action,
			type : 'POST',
			dataType : 'json',
			data : data,
			success : function(resp){
				sweet_alert(resp.type, resp.message);
				if(resp.type == 'success'){
					$(".custom-ajax-form")[0].reset();
					$("#modalForm").modal('hide');
				}
				refreshTable();
			}
		});
	});
});


</script>
@endpush