<?php
namespace Module\Report\Traits;

use Module\Divisi\Models\DivisiModel;
use Module\Karyawan\Models\KaryawanModel;
use Module\Setting\Models\UserModel;
use Module\Report\Models\ReportModel;
use Module\KPI\Models\KPIScoring;

trait ReportTrait
{

	protected function getDivisi(){
		$list = DivisiModel::where('stat', 1)->get();
		return $list->pluck('title', 'id');
	}


	protected function globalVar(){
		$this->periode = date('Y-m').'-01';
		if($this->request->periode){
			$this->periode = date('Y-m-d', strtotime($this->request->periode));
		}
		$this->divisi = $this->request->divisi;
	}


	protected function reportMaker($strict=false){
		$periode = $this->periode;
		$divisi = $this->divisi;

		$data = KaryawanModel::with([
			'getDivisi', 
			'getScore' => function($qry) use($periode){
				$qry->where('periode', $periode);
			}
		])
		->whereHas('getDivisi', function($qry) use($divisi){
			if($divisi){
				$qry->where('id', $divisi);
			}
		})
		->where('stat', 1)
		->get();

		$listPenilai = UserModel::get();


		//simpan ke tabel report
		if(self::periodeExist() && $strict==false){
			return true;
		}
		self::storeToReportTable($data);
	}

	protected function periodeExist(){
		$cek = ReportModel::where('periode', $this->periode)->get();
		if(count($cek) > 0)
			return true;
		return false;
	}



	protected function storeToReportTable($data){
		//clear report di periode x before storing
		ReportModel::where([
			'periode' => $this->periode
		])->delete();

		$k = [];
		foreach($data as $row){
			$penilai = $row->getScore->pluck('id_penilai')->unique();
			$karyawan = $row->id;
			$k[] = $karyawan;
			$divisi = $row->divisi;

			foreach($penilai as $id_penilai){
				$bobot = $row->getScore->where('id_penilai', $id_penilai);
				$nbobot = $skor = 0;
				foreach($bobot as $dt){
					$nbobot += $dt->question->bobot;
					$value = $dt->value - $dt->question->min_threshold;
					$max = $dt->question->max_threshold - $dt->question->min_threshold;
					$skor += ($value / $max) * $dt->question->bobot;
					$index = $skor / $nbobot;
				}

				//store to db
				ReportModel::create([
					'id_karyawan' => $karyawan,
					'id_divisi' => $divisi,
					'id_penilai' => $id_penilai,
					'periode' => $this->periode,
					'skor' => $skor,
					'bobot' => $nbobot,
					'index' => $index,
					'stat' => 1
				]);

			}
		}

		self::calculateKPI($k);

		return true;
	}

	protected function calculateKPI($karyawans){
		foreach($karyawans as $k){
			$instance = ReportModel::where([
				'id_karyawan' => $k,
				'periode' => $this->periode
			]);

			$kpi = 0;
			$get = $instance->get();
			foreach($get as $report){
				$kpi += $report->index;
			}
			if(count($get) > 0){
				$kpi = $kpi / count($get);
			}

			//store KPI score
			$instance->update(['kpi' => $kpi]);
		}
	}





	protected function reportTable($print=false){
		$data = ReportModel::with(
			'karyawan', 
			'divisi', 
			'penilai'
		)->where('periode', $this->periode);
		if($this->divisi){
			$data = $data->where('id_divisi', $this->divisi);
		}

		$data = $data->orderBy('kpi', 'DESC');

		$data = $data->get();

		return view('report::report-table', compact('data', 'print'));
	}


}