<?php
return [
	'General Setting' => [

		'name' => [
			'label' => 'Website Name',
			'description' => 'Nama website yang akan muncul di front end',
			'type' => 'text', //input type
			'default' => NULL,
			'attr' => ['class' => 'form-control'],
		],
		'subtitle' => [
			'label' => 'Website Sub Title',
			'description' => 'Sub judul yang akan muncul dibawah title',
			'list' => [],
			'type' => 'text', //input type
			'default' => 'Powered by Laravel CMS',
			'attr' => ['class' => 'form-control'],
		],

		'copyright' => [
			'label' => 'Copyright Information',
			'description' => '',
			'type' => 'text',
			'default' => 'Copyright &copy; 2017',
			'attr' => ['class' => 'form-control']
		],

		'background' => [
			'label' => 'Backend Login Background',
			'description' => '',
			'type' => 'file',
			'default' => null,
			'attr' => []
		],

		'favicon' =>[
			'label' => 'PNG Favicon',
			'description' => 'File icon format PNG untuk website icon',
			'type' => 'file',
			'default' => null,
			'attr' => []
		],

		'logo' => [
			'label' => 'Website Logo',
			'description' => 'Logo website yang akan ditampilkan',
			'type' => 'file',
			'default' => NULL,
			'attr' => [],
		],

	],


];