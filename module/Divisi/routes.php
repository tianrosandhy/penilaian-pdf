<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('divisi', 'DivisiController@index')->name('admin.divisi.index');
	Route::post('divisi', 'DivisiController@store')->name('admin.divisi.store');
	Route::post('divisi/table', 'DivisiController@getPost');
	Route::get('divisi/switch', 'DivisiController@getSwitch');
	Route::get('divisi/edit', 'DivisiController@edit')->name('admin.divisi.edit');
	Route::post('divisi/update/{id?}', 'DivisiController@update')->name('admin.divisi.update');
	Route::get('divisi/destroy', 'DivisiController@destroy')->name('admin.divisi.destroy');

});
