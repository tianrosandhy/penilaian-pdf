<?php
namespace Module\KPI\Traits;

use Form;
use App\Model\DataTableModel;
use Request;
use Module\KPI\Models\KPIModel;
use Module\KPI\Models\KPIDivisi;

use App\Http\Controllers\Admin\Api\DatatableApi;

trait KPITrait
{

	//Structure Trait

	public function register_column(){
		$this->columns = [
			'Nama KPI' => ['search' => true, 'col' => 'title'],
			'Deskripsi' => ['search' => true, 'col' => 'description'],
			'Created At' => ['search' => true, 'col' => 'created_at'],
			'Divisi' => ['search' => false, 'col' => 'divisi'],
			'Status'	=> ['search' => false, 'col' => 'stat'],
			'' => ['search' => false, 'col' => 'button'], 
		];
	}

    //
    public function register_form(){
		$this->forms = [
			'Nama Group KPI' => Form::input('text', 'title', old('title', null), ['class' => 'form-control']),
			
			'For Divisi' => Form::select('divisi[]', get_list('cms_divisi', 'title'), old('divisi'), ['class' => 'select2 form-control', 'multiple' => 'multiple']),
			
			'Description' => Form::textarea('description', old('description', null), ['class' => 'form-control']),
			'Status' => Form::select('stat', [1 => 'Live', 0 => 'Draft'], old('stat', null), ['class'=>'form-control']),
		];    	
    }



    //Data Table Trait

    public function getPost(){
    	$request = $this->request->all();

    	$dt = new DatatableApi($request, KPIModel::with('list'), $this->columns);
    	$output = $dt->generate(true);
    	$output['data'] = $this->table_format($request, $output);
    	//hapus query result mentah
    	unset($output['result']);

    	return json_encode($output);
    }

    public function table_format($data, $output){
    	if(count($output['result']) == 0){
			$ret = $output['data'];
		}
		else{
			foreach($output['result'] as $row){
				//field index disesuaikan dengan parameter col di this->column
				$exp = explode(",", $row->tags);
				$tags = "";
				foreach($exp as $ex){
					$tags .= '<span class="label label-primary">'.trim($ex)."</span> ";
				}

				$btn = '';

				if(hasAccess('admin.kpi.data.edit')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}

				if(hasAccess('admin.kpi.data.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}

				$ret[] = array(
					$row->title,
					$row->description,
					indo_date($row->created_at),
					self::manageDivisi($row->list),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				);
			}
		}
		return $ret;
	}


	protected function manageDivisi($list){
		$out = [];
		foreach($list as $item){
			$out[] = '<span class="btn btn-tag btn-rounded">'.$item->divisi->title.'</span> ';
		}
		return implode(' ', $out);
	}



	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$data = $this->model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$this->model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}





	protected function storeKPIDivisi($id_kpi_group){
		//delete old if exist
		KPIDivisi::where('id_kpi', $id_kpi_group)->delete();

		foreach($this->request->divisi as $divisi){
			KPIDivisi::create([
				'id_kpi' => $id_kpi_group,
				'id_divisi' => $divisi,
				'stat' => 1
			]);
		}
	}
}