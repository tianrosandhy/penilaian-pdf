@extends('admin.inc.template')

@push ('style')
	{!! register_single('assets/plugins/bootstrap-datepicker/css/datepicker.css', 'css') !!}
@endpush

@section('content')
@if(hasAccess('admin.kpi.data.store'))
<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
	Tambah Data
</a>
<br>
@endif

<hr>

<div class="card">
	<div class="card-block">
		<table class="table data data-table dataTable">
			@include ('admin.inc.datatable-head')
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>


@include ('kpi::create')


@stop

@push('script')
{!! register_single('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', 'js') !!}

<script>
	//initialize datatable
	var tb_data;

	$(function(){
		loadSelect2();

		setTimeout(function(){

		tb_data = $("table.data-table").DataTable({
			'processing': true,
			'serverSide': true,
			'autoWidth' : false,
			'searching'	: false,
			'filter'	: false,
			'ajax'		: {
				type : 'POST',
				url	: ADMIN_URL + '{{ $datatable_ajax['tb'] }}',
				dataType : 'json',
				data : function(data){
					{!! $datatable_search !!}
				}
			},
			"drawCallback": function(settings) {
				$('[data-init="switchery"]').each(function() {
					var el = $(this);
					new Switchery(el.get(0), {
						size : el.data("size")
					});
				});
			},
			"columnDefs" : [
			{
				'targets' : 3,
				'orderable' : false
			},
			{
				'targets' : 4,
				'orderable' : false
			},
			{
				'targets' : 5,
				'orderable' : false
			},

		]
		});

		}, 250);		

	});

	
	//initialize ajax switchable
	$('body').on('change', '.js-switch', function(e) {
		post_id = e.currentTarget.dataset.id;
		if(this.checked) {
			status = 1;
	    } else {
			status = 0;
	    }

		$.ajax({
			type : 'GET',
			dataType : 'json',
			data : {
				id : post_id,
				stat : status
			},
			url : ADMIN_URL + '{{ $datatable_ajax['switch'] }}'
		}).done(function(dt){
			msghandling(dt);
		});
	});




	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-form").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name='"+data[dtt]['name']+"']").val("");
		}
		$(".ajax-form").attr('action', '');
		$(".append").hide();

		$("[name='divisi[]'] option").each(function(){
			$(this).removeAttr('selected');	
		});
		loadSelect2();
		form_refresh();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "{{ $datatable_ajax['edit'] }}",
				type : 'GET',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

					if(nm == 'stat'){
						if(ctn == 1){
							$("input#live").prop('checked', 'checked');
						}
						else{
							$("input#draft").prop('checked', 'checked');
						}
					}

					if(nm == 'list'){
						$list = ctn;
						$("[name='divisi[]'] option").each(function(){
							$(this).removeAttr('selected');	
						});
						vall = new Array();
						$.each($list, function(k, v){
							vall.push(v.id_divisi);
						});
						$("[name='divisi[]']").val(vall);

						loadSelect2();
					}
				});
				form_refresh();

			});

			$(".ajax-form").attr('action', '{{ $edit_action }}/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "{{ $datatable_ajax['delete'] }}",
					type : 'GET',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				});

			}, function(){});
		}
	});


function loadSelect2(){
	$(".select2").select2({
		dropdownParent: $(".modal")
	});
}
</script>
@endpush