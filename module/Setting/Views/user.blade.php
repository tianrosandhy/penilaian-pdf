@extends('admin.inc.template')
@section('content')


<a data-toggle="modal" data-target="#modalForm" class="btn btn-primary btn-add">
	Tambah Data
</a>
<br>

<hr>

<div class="card">
	<div class="card-block">
		<table class="table data data-table dataTable">
			@include ('admin.inc.datatable-head')
			<tbody>
				
			</tbody>
		</table>
	</div>
</div>

@include ('admin.create')


@stop

@section('custom-script')

//tweak sompret mengatasi bug HTTP Not Found, padahal ga ada masalah URL	
var tb_data;
setTimeout(function(){
	//initialize datatable
	tb_data = $("table.data-table").DataTable({
		'processing': true,
		'serverSide': true,
		'autoWidth' : false,
		'searching'	: false,
		'filter'	: false,
		'ajax'		: {
			type : 'POST',
			url	: ADMIN_URL + '{{ $datatable_ajax['tb'] }}',
			dataType : 'json',
			data : function(data){
				{!! $datatable_search !!}
			}
		},
		"drawCallback": function(settings) {
			$('[data-init="switchery"]').each(function() {
				var el = $(this);
				new Switchery(el.get(0), {
					size : el.data("size")
				});
			});
		},
	});
}, 500);	
	

	//add btn
	$(".btn-add").on('click', function(){
		$(".modal-header .modal-title").html("Tambah Data");
		data = $("form.ajax-form").serializeArray();
		for(dtt in data){
			if(data[dtt]['name'] !== '_token');
			$("[name="+data[dtt]['name']+"]").val("");
		}
		$(".ajax-form").attr('action', '');
		$(".row.append").hide();

		form_refresh();
	});


	//edit btn
	$("body").on('click', '.edit-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		$(".modal-header .modal-title").html("Update Data");

		if(post_id > 0){				
			$.ajax({
				url : ADMIN_URL + "{{ $datatable_ajax['edit'] }}",
				type : 'POST',
				dataType : 'json',
				data : {id : post_id}
			}).done(function(dt){
				$.each(dt, function(nm, ctn){
					if($("[name="+nm+"]").length > 0){
						$("[name="+nm+"]").val(ctn);
					}

				});
				form_refresh();

			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});

			$(".ajax-form").attr('action', '{{ $edit_action }}/'+post_id);

		}
	});


	//delete btn
	$("body").on('click', '.delete-btn', function(){
		var ini = $(this);
		post_id = ini.data('id');

		if(post_id > 0){
			alertify.confirm('Hapus Data?', 'Apakah anda-benar-benar akan menghapus data ini?', function(){
				
				$.ajax({
					url : ADMIN_URL + "{{ $datatable_ajax['delete'] }}",
					type : 'POST',
					dataType : 'json',
					data : {id : post_id}
				}).done(function(dt){
					msghandling(dt);
					tb_data.ajax.reload();
				}).fail(function(dt){
					swal('Error ' + dt.status, dt.responseText, 'warning');
				});

			}, function(){});
		}
	});


@stop