<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiToDivisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_kpi_to_divisi', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_kpi');
            $tb->integer('id_divisi');
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_kpi_to_divisi');
    }
}
