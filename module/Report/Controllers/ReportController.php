<?php
namespace Module\Report\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Module\Report\Traits\ReportTrait;
use Module\Report\Models\ReportModel;
use Module\KPI\Models\KPIScoring;

use Mpdf\Mpdf;

class ReportController extends Controller
{
	use ReportTrait;

	public 
		$request,
		$periode,
		$divisi;

	public function __construct(Request $req){
		$this->request = $req;
	}

	public function index(){
		$title = 'Report Data';
		$periode = date('F Y');
		if(isset($this->request->periode)){
			$periode = date('F Y', strtotime($this->request->periode));
		}

		$divisi = self::getDivisi();
		$selected = [
			'periode' => $periode,
			'divisi' => $this->request->divisi
		];


		self::globalVar();
		$reportMaker = self::reportMaker($this->request->strict); //strict mode false
		$tableData = self::reportTable();

		return view('report::index', compact(
			'title',
			'periode',
			'divisi',
			'selected',
			'tableData'
		));
	}

	public function print(){
		self::globalVar();
		$data = self::reportTable(true);
		$title = 'Laporan Score KPI';

		$divisi = self::getDivisi();
		$dvname = '<em>Seluruh Divisi</em>';
		$subtitle = '';
		if($this->request->divisi){
			if(isset($divisi[$this->request->divisi])){
				$dvname = $divisi[$this->request->divisi];
				$subtitle .= ' Divisi '. $dvname;
			}
		}

		$periode = strlen($this->request->periode) == 0 ? date('M Y') : $this->request->periode;
		if(isset($this->request->periode)){
			$subtitle .= ' Periode '. $periode;
		}

		$content = view('report::print', compact(
			'data', 
			'title',
			'subtitle',
			'dvname',
			'periode'
		))->render();

		$mpdf = new Mpdf();
		$mpdf->WriteHTML($content);
		$mpdf->Output();

	}

	public function detail($id=0){
		$data = ReportModel::with('karyawan', 'divisi', 'penilai')->findOrFail($id);
		$title = 'Scoring Detail';

		$score = KPIScoring::with('question')->where([
			'id_user' => $data->id_karyawan,
			'periode' => $data->periode,
			'id_penilai' => $data->id_penilai,
			'stat' => 1
		])->get();

		$view = view('report::detail', compact(
			'data',
			'title',
			'score'
		))->render();

		$mpdf = new Mpdf();
		$mpdf->WriteHTML($view);
		$mpdf->Output();
	}
}