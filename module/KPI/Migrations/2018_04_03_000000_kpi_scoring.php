<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiScoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_kpi_scoring', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_user');
            $tb->integer('id_question');
            $tb->integer('id_penilai');
            $tb->integer('value');
            $tb->date('periode');
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_kpi_scoring');
    }
}
