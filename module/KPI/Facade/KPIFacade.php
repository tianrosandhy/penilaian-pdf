<?php 

namespace Module\KPI\Facade;

use Illuminate\Support\Facades\Facade;

class KPIFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\KPI\\Controllers\\KPIController';
    }
}