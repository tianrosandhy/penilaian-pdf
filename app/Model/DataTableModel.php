<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;

class DataTableModel extends Model
{
    //
    public $table;
    public $fillable;
    public $searchable;
    public $data;

    public function __construct($table = 'cms_post', $searchable = []){
    	$this->table = $table;
    	$src = [];
    	foreach($searchable as $lbl => $data){
    		if($data['search']){
    			$src[] = $data['col'];
    		}
    	}
    	$this->searchable = $src;
    }


    public function get_data($withstat=true){
    	if($withstat)
	    	$all = self::where('stat','<>',9)->get();
	    else
	    	$all = self::get();

		$out['recordsTotal'] = count($all);

		if($this->data['draw'] > 0){
			//cari seluruh post dengan filter apabila ada
			if($withstat)
		    	$post = self::where('stat','<>',9);
		    else
		    	$post = self::where('id','>',0);

			foreach($this->searchable as $src){
				if(strlen($this->data[$src]) > 0){
					$post = $post->where($src, 'LIKE', '%'.$this->data[$src].'%');
				}
			}

			$indexsearch = $this->data['order'][0]['column'];
			$arah = $this->data['order'][0]['dir'];

			if(isset($this->searchable[$indexsearch]))
			$post = $post->orderBy($this->searchable[$indexsearch], $arah);
			
			$post = $post->offset($this->data['start'])->limit($this->data['length'])->get();
			//ada

			$out['draw'] = $this->data['draw'];
			$out['recordsFiltered'] = count($all);
			$out['result'] = $post;

			return $out;
		}
		else{
			return $this->blank_action();
		}
    }

    public function blank_action(){
		$rt = [];
		for($i=0; $i<count($this->data['columns']); $i++){
			array_push($rt, '');
		}
		$ret[] = $rt;
		return $ret;
	}


	static function get_from_tb($tbname, $id, $get, $field='id'){
		$cek = DB::table($tbname)
		->where('stat','<>',9)
		->where($field, $id)
		->first();

		if(count($cek) > 0){
			$row = (array)$cek;
			return $row[$get];
		}
		else
			return null;
	}

    static function generate_search($data){
    	$out = [];
		foreach($data as $dt){
			if($dt['search'])
				$out[] = "data.".$dt['col']." = $('#". $dt['col'] ."').val()";
		}
		return implode(", ", $out);
    }


    static function customComboBox($name, $tb, $label, $pk, $where=[]){
    	$data = DB::table($tb)
    		->where('stat', 1);

    	if(count($where) > 0){
    		$data = $data->where($where);
    	}

    	$data = $data->get()->pluck($label, $pk)->toArray();
    	return self::manualComboBox($name, $data);
    }

    static function manualComboBox($name, $data=[]){
    	$html = '<select name="'.$name.'" class="form-control search" id="'.$name.'">';
    	$html .= '<option value="">Search '.ucfirst($name).'</option>';
    	foreach($data as $id => $val){
    		$html .= '<option value="'.$id.'">'.$val.'</option>';
    	}
    	$html .= '</select>';

    	return $html;
    }

}
