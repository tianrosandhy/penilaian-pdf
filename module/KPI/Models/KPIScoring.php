<?php
namespace Module\KPI\Models;

use Illuminate\Database\Eloquent\Model;
use Request;

class KPIScoring extends Model
{
    //
    protected $table = "cms_kpi_scoring";
    protected $fillable = [
    	'id_user',
    	'id_question',
        'id_penilai',
    	'value',
    	'periode',
    	'stat'
    ];

    public function user(){
    	return $this->belongsTo('Module\Karyawan\Models\KaryawanModel', 'id_user');
    }

    public function question(){
    	return $this->belongsTo('Module\KPI\Models\KPIQuestion', 'id_question');
    }
    
    public function penilai(){
        return $this->belongsTo('Module\Setting\Models\UserModel', 'id_penilai');
    }
}
