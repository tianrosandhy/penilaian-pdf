-- --------------------------------------------------------
-- Host:                         localhost
-- Versi server:                 5.7.19 - MySQL Community Server (GPL)
-- OS Server:                    Win64
-- HeidiSQL Versi:               9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_divisi
CREATE TABLE IF NOT EXISTS `cms_divisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ord` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_divisi: ~0 rows (lebih kurang)
DELETE FROM `cms_divisi`;
/*!40000 ALTER TABLE `cms_divisi` DISABLE KEYS */;
INSERT INTO `cms_divisi` (`id`, `title`, `image`, `description`, `ord`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 'Pemasaran', NULL, 'Divisi pemasaran yang jualan', 0, '2018-05-23 10:05:46', '2018-05-23 10:05:46', 1),
	(2, 'Marketing', NULL, 'Orang yang suka nawar-nawarin produk', 0, '2018-05-23 10:05:58', '2018-05-23 10:05:58', 1);
/*!40000 ALTER TABLE `cms_divisi` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_karyawan
CREATE TABLE IF NOT EXISTS `cms_karyawan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jk` tinyint(4) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `divisi` int(11) DEFAULT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ord` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_karyawan: ~0 rows (lebih kurang)
DELETE FROM `cms_karyawan`;
/*!40000 ALTER TABLE `cms_karyawan` DISABLE KEYS */;
INSERT INTO `cms_karyawan` (`id`, `title`, `slug`, `tempat`, `tgl_lahir`, `jk`, `email`, `telp`, `hp`, `image`, `description`, `divisi`, `jabatan`, `ord`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 'Christian Rosandhy', 'christian-rosandhy', 'Denpasar', '1995-12-07', 1, 'tianrosandhy@gmail.com', '089622224614', NULL, NULL, '<p>lorem ipsum dolor sit amet</p>', 1, 'Programmer', 0, '2018-05-23 10:06:32', '2018-05-23 10:06:32', 1);
/*!40000 ALTER TABLE `cms_karyawan` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_kpi
CREATE TABLE IF NOT EXISTS `cms_kpi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ord` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_kpi: ~0 rows (lebih kurang)
DELETE FROM `cms_kpi`;
/*!40000 ALTER TABLE `cms_kpi` DISABLE KEYS */;
INSERT INTO `cms_kpi` (`id`, `title`, `description`, `ord`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 'General KPI', 'KPI untuk umum', 0, '2018-05-23 10:07:00', '2018-05-23 10:07:00', 1);
/*!40000 ALTER TABLE `cms_kpi` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_kpi_question
CREATE TABLE IF NOT EXISTS `cms_kpi_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kpi_group` int(11) NOT NULL,
  `statement` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobot` int(11) NOT NULL,
  `min_threshold` int(11) NOT NULL DEFAULT '0',
  `max_threshold` int(11) NOT NULL DEFAULT '100',
  `ord` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_kpi_question: ~0 rows (lebih kurang)
DELETE FROM `cms_kpi_question`;
/*!40000 ALTER TABLE `cms_kpi_question` DISABLE KEYS */;
INSERT INTO `cms_kpi_question` (`id`, `id_kpi_group`, `statement`, `bobot`, `min_threshold`, `max_threshold`, `ord`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 1, 'Keramahtamahan', 50, 0, 100, NULL, '2018-05-23 10:07:23', '2018-05-23 10:07:23', 1),
	(2, 1, 'Cekatan dalam berjualan', 25, 0, 100, NULL, '2018-05-23 10:07:38', '2018-05-23 10:07:38', 1),
	(3, 1, 'Kebersihan diri dan ruangan', 10, 0, 100, NULL, '2018-05-23 10:07:48', '2018-05-23 10:07:48', 1),
	(4, 1, 'Jam makan siang yang teratur', 15, 0, 100, NULL, '2018-05-23 10:07:59', '2018-05-23 10:07:59', 1);
/*!40000 ALTER TABLE `cms_kpi_question` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_kpi_scoring
CREATE TABLE IF NOT EXISTS `cms_kpi_scoring` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `id_penilai` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `periode` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_kpi_scoring: ~0 rows (lebih kurang)
DELETE FROM `cms_kpi_scoring`;
/*!40000 ALTER TABLE `cms_kpi_scoring` DISABLE KEYS */;
INSERT INTO `cms_kpi_scoring` (`id`, `id_user`, `id_question`, `id_penilai`, `value`, `periode`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 1, 1, 1, 53, '2018-05-01', '2018-05-23 10:08:31', '2018-05-23 10:08:31', 1),
	(2, 1, 2, 1, 74, '2018-05-01', '2018-05-23 10:08:31', '2018-05-23 10:08:31', 1),
	(3, 1, 3, 1, 78, '2018-05-01', '2018-05-23 10:08:31', '2018-05-23 10:08:31', 1),
	(4, 1, 4, 1, 28, '2018-05-01', '2018-05-23 10:08:31', '2018-05-23 10:08:31', 1);
/*!40000 ALTER TABLE `cms_kpi_scoring` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_kpi_to_divisi
CREATE TABLE IF NOT EXISTS `cms_kpi_to_divisi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kpi` int(11) NOT NULL,
  `id_divisi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_kpi_to_divisi: ~0 rows (lebih kurang)
DELETE FROM `cms_kpi_to_divisi`;
/*!40000 ALTER TABLE `cms_kpi_to_divisi` DISABLE KEYS */;
INSERT INTO `cms_kpi_to_divisi` (`id`, `id_kpi`, `id_divisi`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 1, 1, '2018-05-23 10:07:00', '2018-05-23 10:07:00', 1),
	(2, 1, 2, '2018-05-23 10:07:00', '2018-05-23 10:07:00', 1);
/*!40000 ALTER TABLE `cms_kpi_to_divisi` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_login_log
CREATE TABLE IF NOT EXISTS `cms_login_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `server_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_login_log: ~0 rows (lebih kurang)
DELETE FROM `cms_login_log`;
/*!40000 ALTER TABLE `cms_login_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_login_log` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.cms_report
CREATE TABLE IF NOT EXISTS `cms_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_karyawan` int(11) NOT NULL,
  `id_divisi` int(11) NOT NULL,
  `id_penilai` int(11) NOT NULL,
  `periode` date NOT NULL,
  `skor` double(5,2) NOT NULL,
  `bobot` int(11) DEFAULT NULL,
  `index` double(5,2) DEFAULT NULL,
  `kpi` double(5,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.cms_report: ~0 rows (lebih kurang)
DELETE FROM `cms_report`;
/*!40000 ALTER TABLE `cms_report` DISABLE KEYS */;
INSERT INTO `cms_report` (`id`, `id_karyawan`, `id_divisi`, `id_penilai`, `periode`, `skor`, `bobot`, `index`, `kpi`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 1, 1, 1, '2018-05-01', 57.00, 100, 0.57, 0.57, '2018-05-23 10:08:38', '2018-05-23 10:08:38', 1);
/*!40000 ALTER TABLE `cms_report` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.migrations: ~11 rows (lebih kurang)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_000000_login_log', 1),
	(3, '2017_10_19_031523_priviledge', 1),
	(4, '2017_10_19_040518_setting', 1),
	(5, '2017_10_20_075012_divisi', 1),
	(6, '2017_10_20_075012_karyawan', 1),
	(7, '2017_10_20_075012_kpi', 1),
	(8, '2018_03_31_000000_kpi_to_divisi', 1),
	(9, '2018_04_01_000000_kpi_question', 1),
	(10, '2018_04_03_000000_kpi_scoring', 1),
	(11, '2018_04_05_000000_cms_report', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.setting
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `param` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.setting: ~7 rows (lebih kurang)
DELETE FROM `setting`;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` (`id`, `param`, `value`, `created_at`, `updated_at`, `stat`) VALUES
	(1, 'name', 'Sistem Penilaian Karyawan', '2018-05-03 12:12:55', '2018-05-03 12:15:24', 1),
	(2, 'subtitle', 'Powered by Laravel CMS', '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(3, 'copyright', 'Copyright © 2018', '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(4, 'background', NULL, '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(5, 'favicon', NULL, '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(6, 'logo', NULL, '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(7, 'login_attempt', '10', '2018-05-03 12:12:55', '2018-05-03 12:15:25', 1),
	(8, 'custom_script', '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42132853-3"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-42132853-3\');\r\n</script>', '2018-05-03 12:15:25', NULL, 1);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priviledge` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.users: ~0 rows (lebih kurang)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `priviledge`) VALUES
	(1, 'admin', 'admin@localhost', '$2y$10$V7JSoAjBfyyC5hwL5gEFHOWpgsIVVdPrFskGDlLSgGjpewdNVczUC', 'vKqV6YEdR1Nw2xeQz6EGaf0YrnxofYKVqUkMhODOziPbY8VwHN4wH2qkXI16', NULL, NULL, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- membuang struktur untuk table 2018_penilaian_v2.users_priviledge
CREATE TABLE IF NOT EXISTS `users_priviledge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `priviledge_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permission` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `stat` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Membuang data untuk tabel 2018_penilaian_v2.users_priviledge: ~0 rows (lebih kurang)
DELETE FROM `users_priviledge`;
/*!40000 ALTER TABLE `users_priviledge` DISABLE KEYS */;
INSERT INTO `users_priviledge` (`id`, `priviledge_name`, `permission`, `created_at`, `updated_at`, `type`, `stat`) VALUES
	(1, 'Administrator', '["karyawan.index","karyawan.store","karyawan.edit","karyawan.update","karyawan.destroy","divisi.index","divisi.store","divisi.edit","divisi.update","divisi.destroy","kpi.data.index","kpi.data.store","kpi.data.edit","kpi.data.update","kpi.data.destroy","kpi.question.index","kpi.question.store","kpi.question.update","kpi.question.destroy","kpi.scoring.index","kpi.scoring.store","report.index","setting.setting.index","setting.setting.store","setting.user.index","setting.user.store","setting.user.edit","setting.user.update","setting.user.destroy","setting.priviledge.index","setting.priviledge.store","setting.priviledge.edit","setting.priviledge.update","setting.priviledge.manage","setting.priviledge.destroy"]', NULL, '2018-05-03 11:54:33', 0, 1);
/*!40000 ALTER TABLE `users_priviledge` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
