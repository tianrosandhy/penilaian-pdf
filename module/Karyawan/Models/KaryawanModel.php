<?php

namespace Module\Karyawan\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class KaryawanModel extends Model
{
    //
    protected $table = "cms_karyawan";
    protected $fillable = [
    	'title',
    	'slug',
        'tempat',
        'tgl_lahir',
        'jk',
        'email',
        'telp',
        'hp',
    	'image',
        'description',
        'divisi',
        'jabatan',
        'ord',
    	'stat'
    ];


    public function getDivisi(){
        return $this->belongsTo('Module\Divisi\Models\DivisiModel', 'divisi');
    }


    public function getScore(){
        return $this->hasMany('Module\KPI\Models\KPIScoring', 'id_user');
    }

}
