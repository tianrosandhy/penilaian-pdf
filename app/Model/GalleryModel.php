<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Input;
use Image;

class GalleryModel extends Model
{
    //
    public $resize_to = [];
    public $dir = "/";
    public $disk;
    public $file;
    public $filename;


    public function __construct($arr){
    	$this->resize_to = $arr;
    	$this->disk = config('filesystems.disks.local.root');
    }

    public function upload($file){
    	$this->file = $file;
    	if($this->file->isValid()){
    		//upload ke direktori utama
    		$this->filename = $file->store($this->dir);
    		$this->resize();
    		return true;
    	}
    	else{
    		return false;
    	}
    }

    public function resize(){
    	$imgname = $this->filename;
    	foreach($this->resize_to as $batas){

    		$image = Image::make($this->disk."/".$imgname);
    		$image->resize($batas, null, function($const){
    			$const->aspectRatio();
    		});
    		$image->save($this->disk.$this->dir."thumb/thumb-".$batas."-".$imgname);
    	}
    }

    static function rollback($filename, $dir="/"){
        $disk = config('filesystems.disks.local.root');
        if(is_file($disk.$dir.$filename)){
            unlink($disk.$dir.$filename);
        }
        $list = glob($disk.$dir."thumb/*");
        foreach($list as $l){
            if(preg_match("/w*".$filename."/", $l)){
                unlink($l);
            }
        }

    }

}
