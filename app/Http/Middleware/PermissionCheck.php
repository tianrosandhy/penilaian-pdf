<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Illuminate\Routing\Router;
use Auth;

class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $route;
    protected $except = [
        '',
        'dashboard',
        'login',
        'postlogin',
        'logout',

    ];

    public function __construct(Router $router, Auth $auth){
        $this->router = $router;
    }

    public function handle($request, Closure $next, $guard=null)
    {
        if($request->ajax())
            return $next($request);

        $default_url = env('ADMIN_PREFIX');


        $action_name = ($this->router->getRoutes()->match($request)->getName()!=null) ? $this->router->getRoutes()->match($request)->getName() : '';
        $action_name = str_replace("admin.", "", $action_name);
        if(in_array($action_name, $this->except)){
            return $next($request);
        }


        $level = Auth::user()['priviledge'];
        $get = DB::table('users_priviledge')
            ->where('id', $level)
            ->first();

        if(count($get) == 1){
            $list_permission = json_decode($get->permission, true);

            if(in_array($action_name, $list_permission)){
                return $next($request);
            }
            else{
                return redirect($default_url)->with([
                    'error' => 'You have no access to this page'
                ]);
            }
        }
        else{
            return redirect($default_url);
        }
    }
}
