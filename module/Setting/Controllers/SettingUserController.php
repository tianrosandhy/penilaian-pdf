<?php

namespace Module\Setting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use App\Model\DataTableModel;
use Form;
use Validator;
use Auth;

use Module\Setting\Models\UserModel;
use Module\Setting\Traits\UserTrait;


class SettingUserController extends Controller
{
	use UserTrait;

	public 	$request, 
			$response,
			$columns,
			$forms,
			$model;
	

    //
	public function __construct(Request $req, Response $red){
		$this->request = $req;
		$this->response = $red;
		$this->model = new UserModel();

		$this->register_column();
	}

    public function index(){
		$this->register_form();

    	return view('setting::user')->with([
    		'title' => 'User Setting',
    		'columns' => $this->columns,
    		'forms' => $this->forms,
    		'datatable_search' => DataTableModel::generate_search($this->columns),
    		'datatable_ajax' => [
    			'tb' => '/setting/user/table',
    			'switch' => '/setting/user/switch',
    			'edit' => '/setting/user/edit',
    			'delete' => '/setting/user/destroy'
    		],
    		'edit_action' => route('admin.setting.user.update'),
    		'additional_css' => config('asset.group.index')['css'],
    		'additional_js' => config('asset.group.index')['js'],
    	]);

    }

    public function store(){
    	$rule = [
    		'username' => 'required|max:60',
    		'email' => 'required|email',
    		'pass' => 'required|min:5|confirmed',
    		'priviledge' => 'required|integer',
    	];

    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('username', $this->request->username)
			->get();

		if(count($cek) > 0){
			$out['error'][][0] = 'Data user dengan email / username tersebut sudah ada.';
		}
		else{
			$pass = password_hash($this->request->pass, PASSWORD_DEFAULT);
			$remember = sha1($pass);
			//proses simpan
			$this->model->insert([
				'username' => $this->request->username,
				'email' => $this->request->email,
				'password' => $pass,
				'remember_token' => $remember,
				'created_at' => date("Y-m-d H:i:s"),
				'priviledge' => intval($this->request->priviledge)
			]);
			$out['success'] = "Berhasil menyimpan data user";
		}

    	return json_encode($out);

    }



    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function update($id){
    	$rule = [
    		'username' => 'required|max:60',
    		'email' => 'required|email',
    		'priviledge' => 'required|integer',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('username', $this->request->username)
			->where('id','<>',$id)
			->get();

		if(count($cek) > 0){
			$out['error'][][0] = 'Data user dengan email / username tersebut sudah ada.';
		}
		else{
			$data = [
				'username' => $this->request->username,
				'email' => $this->request->email,
				'priviledge' => intval($this->request->priviledge)
			];
			if(strlen($this->request->pass) > 0 || strlen($this->request->pass_confirmation) > 0){

				$pass1 = $this->request->pass;
				$pass2 = $this->request->pass_confirmation;

				if($pass1 <> $pass2){
					$out['error'][][0] = 'Password mismatch.';
				}
				elseif(strlen($pass1) < 5){
					$out['error'][][0] = "The password need at least 5 character";
				}
				else{
					$pass = password_hash($pass1, PASSWORD_DEFAULT);
					$data['password'] = $pass;
				}
			}

			//proses simpan
			if(!isset($count['error'])){
				$this->model->where('id',$id)->update($data);
				$out['success'] = "Berhasil mengupdate data user";
			}

		}

    	return json_encode($out);
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

}
