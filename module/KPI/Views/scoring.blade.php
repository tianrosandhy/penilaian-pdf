@extends('admin.inc.template')
@push ('style')
	{!! register_single('assets/plugins/bootstrap-datepicker/css/datepicker.css', 'css') !!}
@endpush

@section ('content')
<div class="row">
	<div class="col-lg-4 col-sm-6">
		<div class="form-group">
			<label>Periode Scoring</label>
			<input type="text" value="{{ $periode }}" class="periode-timepicker form-control">
		</div>
	</div>
</div>



<table class="data table">
	<thead>
		<tr>
			<th>Nama Karyawan</th>
			<th>Divisi</th>
			<th>Jabatan</th>
			<th>Score</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>


@include ('kpi::partials.scoring-modal')

@stop


@push('script')
{!! register_single('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', 'js') !!}
<script>
var tb_data;
$(function(){
	$(".periode-timepicker").datepicker({
	    format: "MM yyyy",
	    startView: 2,
	    minViewMode: 1
	});

	$(".periode-timepicker").on('change', function(){
		val = $(this).val();
		location.href = window.location.origin + window.location.pathname + '?periode=' + val;
	});


	setTimeout(function(){
	tb_data = $("table.data").DataTable({
		'processing': true,
		'serverSide': true,
		'autoWidth' : false,
		'searching'	: false,
		'filter'	: false,
		'ajax'		: {
			type : 'POST',
			url	: ADMIN_URL + '/kpi/scoring/table',
			dataType : 'json',
			data : {
				periode : '{{ $periode }}'
			}
		},
		"drawCallback": function(settings) {
			$('[data-init="switchery"]').each(function() {
				var el = $(this);
				new Switchery(el.get(0), {
					size : el.data("size")
				});
			});
		},
		"columnDefs" : [
			{
				'targets' : 3,
				'orderable' : false
			},
			{
				'targets' : 4,
				'orderable' : false
			},
		]
	});	}, 250);


});
</script>
@endpush