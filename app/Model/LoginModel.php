<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use Setting;
use App\Model\LoginLogModel;
use MailHelper;

class LoginModel extends Model
{
    public $data;
    protected $table = 'users';
    protected $target;


    public function __construct($post){
        $this->data = $post;
        $this->target = env('ADMIN_PREFIX');
    }


    public function attempt(){
        //cek limit login dulu
        $email = $this->data['username'];
        $remember = isset($this->data['remember']) ? ($this->data['remember'] == 1 ? true : false ) : false;        

        $limit = intval(Setting::getParam('login_attempt'));
        $batas = time() - (24 * 60 * 60); //1 hari
        $cek = LoginLogModel::where('stat', 'fail')
            ->where('created_at', '>', $batas)
            ->where('username', $email)
            ->get();

        if(count($cek) >= $limit){
            //ga boleh login
            return ['info' => 'Too many wrong log in action. You cannot login again until tommorow'];
        }

        if(Auth::guard('web')->attempt(['username' => $email, 'password' => $this->data['password'] ], $remember)){
            //sukses
            return ['success' => 'Login Success'];
        }
        else if(Auth::guard('web')->attempt(['email' => $email, 'password' => $this->data['password'] ], $remember)){
            return ['success' => 'Login Success'];
        }
        else{
            //hmm
            if(empty($this->data['username']) or empty($this->data['password'])){
                $msg = "Blank username or password";
            }
            else{
                $msg = "Wrong login detail. Please try again";
            }
            return ['error' => $msg];
        }
    }

}
