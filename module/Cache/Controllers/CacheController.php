<?php

namespace Module\Cache\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Auth;
use Cache;



class CacheController extends Controller
{
	public static
		$request,
		$data;
	

    //
	public function __construct(Request $req){
		self::$request = $req;
		self::$data = [
			'dir' => '',
			'type' => 'css',
			'save_location' => 'cache/'
		];
	}

	static function merge($data=[], $config=[], $force=false, $save=true){
		//kalau ada config diinput, langsung proses sebelum dimerge
		if(count($config) > 0){
			self::config($config);
		}

		//pemberian nama hash cache
		$cachename = self::get_cache_name($data);
		$target_file = self::$data['save_location'].self::$data['type'].'/'.$cachename;

		if(is_file($target_file) || $force){
			//kalau cache sudah ada, langsung baca konten cache ybs
			$file = fopen($target_file, 'r');
			$output = fread($file, filesize($target_file));
		}
		else{
			//kalau cache blm ada, buat baru berdasarkan inputan
			$output = '';
			foreach($data as $item){
				$output .= "/* ".$item." */
".self::get_content($item)."

";
			}
			$file = fopen($target_file, 'w');
			fwrite($file, $output);
			fclose($file);
		}


		if($save){
			//hanya buat cache jika file yg bersangkutan belum ada
			//atau jika ingin dipaksa update
			
			if(self::$data['type'] == 'js'){
				return '<script src="'.url($target_file).'"></script>';
			}
			else if(self::$data['type'] == 'css'){
				return '<link rel="stylesheet" href="'.url($target_file).'">';
			}
		}

		return $output;
	}

	static function get_content($item){
		$url = self::$data['dir']."/".$item;
		$content = "";
		if(is_file($url)){
			$file = fopen($url, "r");
			$content = fread($file, filesize($url));
			fclose($file);
		}
		else{
			$content = '/* File '.$url.' is not found */
';
		}
		return $content;
	}

	static function config($data){
		foreach($data as $key => $value){
			self::$data[$key] = $value;
		}
	}

	static function get_cache_name($data, $withtime=false){
		//cache name akan berganti setiap jam
		$time = $withtime ? date("Y-m-d-H-") : "";
		$filename = [];
		foreach($data as $file){
			$exp = explode("/", $file);
			$n = count($exp) - 1;
			$filename[] = $exp[$n];
		}

		$impl = implode("-",$filename);
		$cachename = sha1($time.$impl).".".self::$data['type'];
		return $cachename;
	}

}
