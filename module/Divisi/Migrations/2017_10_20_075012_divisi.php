<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Divisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_divisi', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->string('image',190)->nullable();
            $tb->string('description')->nullable();
            $tb->integer('ord');
            $tb->timestamps();            
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cms_divisi');
    }
}
