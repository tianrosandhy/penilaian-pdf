<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KPI extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_kpi', function(Blueprint $tb){
            $tb->increments('id');
            $tb->string('title',190);
            $tb->string('description')->nullable();
            $tb->integer('ord');
            $tb->timestamps();            
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('cms_kpi');
    }
}
