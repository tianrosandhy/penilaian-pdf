<?php
return [
	'menu' => [
		'Dashboard' => [
			'target' => 'admin.dashboard',
			'icon' => 'fa-home'
		],

		'Master Data' => [
			'target' => null,
			'icon' => 'fa-database',
			'submenu' => [
				'Karyawan' => 'admin.karyawan.index',
				'Divisi' => 'admin.divisi.index'
			]
		],

		'KPI' => [
			'target' => null,
			'icon' => 'fa-table',
			'submenu' => [
				'Group' => 'admin.kpi.data.index',
				'Question' => 'admin.kpi.question.index',
				'Scoring' => 'admin.kpi.scoring.index'
			]
		],

		'Report' => [
			'target' => 'admin.report.index',
			'icon' => 'fa-bar-chart'
		],

		'Setting' => [
			'target' => null,
			'icon' => 'fa-cog',
			'submenu' => [
				'General' => 'admin.setting.setting.index',
				'User' => 'admin.setting.user.index',
				'Priviledge' => 'admin.setting.priviledge.index',
			]
		],
		"Logout" => [
			'target' => 'admin.logout',
			'icon' => 'fa-sign-out'
		]


	],

];