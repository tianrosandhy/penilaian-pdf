<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>{{ isset($title) ? $title : '' }}</title>
{!! register_single('assets/plugins/bootstrap/css/bootstrap.css', 'css') !!}
<style>
	body{padding:1.5cm;}
	h1{font-size:20px; line-height:1em; text-transform:uppercase; margin:0; padding:0;}
</style>
</head>
<body>

<h1>{{ isset($title) ? $title : '' }}</h1>
<hr>
@yield('content')
	
</body>
</html>