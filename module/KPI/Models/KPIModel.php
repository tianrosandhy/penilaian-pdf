<?php

namespace Module\KPI\Models;

use Illuminate\Database\Eloquent\Model;
use Request;
use Auth;
use Redirect;
use DB;
use Form;


class KPIModel extends Model
{
    //
    protected $table = "cms_kpi";
    protected $fillable = [
    	'title',
    	'image',
        'description',
        'ord',
    	'stat'
    ];

    public function list(){
    	return $this->hasMany('Module\KPI\Models\KPIDivisi', 'id_kpi');
    }

    public function question(){
        return $this->hasMany('Module\KPI\Models\KPIQuestion', 'id_kpi_group');
    }


}
