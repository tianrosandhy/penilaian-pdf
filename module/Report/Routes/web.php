<?php
$router->group(['prefix' => 'report'], function(){
	Route::get('/', 'ReportController@index')->name('admin.report.index');
	Route::get('print', 'ReportController@print');
	Route::get('detail/{id}', 'ReportController@detail');
});