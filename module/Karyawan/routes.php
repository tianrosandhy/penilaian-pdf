<?php
$router->group(['prefix' => env('ADMIN_PREFIX')], function($route){
	Route::get('karyawan', 'KaryawanController@index')->name('admin.karyawan.index');
	Route::post('karyawan', 'KaryawanController@store')->name('admin.karyawan.store');
	Route::post('karyawan/table', 'KaryawanController@getPost');
	Route::get('karyawan/switch', 'KaryawanController@getSwitch');
	Route::get('karyawan/edit', 'KaryawanController@edit')->name('admin.karyawan.edit');
	Route::post('karyawan/update/{id?}', 'KaryawanController@update')->name('admin.karyawan.update');
	Route::get('karyawan/destroy', 'KaryawanController@destroy')->name('admin.karyawan.destroy');

});
