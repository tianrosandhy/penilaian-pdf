<?php

namespace Module\Divisi\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use App\Model\DataTableModel;
use Form;
use Validator;
use Auth;
use Ticmi;
use App\Model\GalleryModel;

use Module\Divisi\Models\DivisiModel;
use Module\Divisi\Traits\DivisiTrait;


class DivisiController extends Controller
{
	public 	$request, 
			$response,
			$columns,
			$forms,
			$model;
	
	use DivisiTrait;

    //
	public function __construct(Request $req, Response $red){
		$this->request = $req;
		$this->response = $red;
		$this->model = new DivisiModel();
		
		$this->register_column();
	}

    public function index(){
		$this->register_form();

    	return view('divisi::index')->with([
    		'title' => 'Divisi',
    		'columns' => $this->columns,
    		'forms' => $this->forms,
    		'datatable_search' => DataTableModel::generate_search($this->columns),
    		'datatable_ajax' => [
    			'tb' => '/divisi/table',
    			'switch' => '/divisi/switch',
    			'edit' => '/divisi/edit',
    			'delete' => '/divisi/destroy'
    		],
    		'edit_action' => route('admin.divisi.update'),
    		'jk_box' => DataTableModel::manualComboBox('jk', [
    			1 => 'Pria',
    			2 => 'Wanita'
    		])
    	]);

    }

    
    public function store(){
    	$rule = [
    		'title' => 'required|max:255',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data divisi dengan nama tersebut sudah ada. Mohon gunakan nama lain agar mudah dibedakan";

		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$this->model->create([
				'title' => $this->request->title,
				'image' => $this->request->uploaded_img,
				'description' => $this->request->description,
				'ord' => intval($this->request->ord),
				'stat' => intval($this->request->stat)
			]);
			$out['success'] = "Berhasil menyimpan data divisi";
		}

    	return json_encode($out);

    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:255',
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data post dengan judul / slug tersebut sudah ada.";

		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'title' => $this->request->title,
				'description' => $this->request->description,
				'stat' => $this->request->stat,
			];

			if($this->request->uploaded_img){
				$list_update['image'] = $this->request->uploaded_img;
			}

			$this->model->where('id', $id)->update($list_update);
			$out['success'] = "Berhasil mengupdate data divisi";
		}

    	return json_encode($out);
    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$delimg = $cek->image;
	    			GalleryModel::rollback($delimg);

		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    static function getByName($name,  $default=''){
    	//get Divisi URL by name
    	$sql = DivisiModel::where('title', $name)
    		->where('stat', 1)
    		->first();
    	if(count($sql) > 0){
    		return url('upload/'.$sql->image);
    	}
    	return $default;
    }
}
