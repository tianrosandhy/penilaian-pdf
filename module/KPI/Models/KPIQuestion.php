<?php
namespace Module\KPI\Models;

use Illuminate\Database\Eloquent\Model;
use Request;

class KPIQuestion extends Model
{
    //
    protected $table = "cms_kpi_question";
    protected $fillable = [
    	'id_kpi_group',
    	'statement',
    	'bobot',
    	'min_threshold',
    	'max_threshold',
        'ord',
    	'stat'
    ];

    public function groupKpi(){
    	return $this->belongsTo('Module\KPI\Models\KPIModel', 'id_kpi_group');
    }

    
}
