<?php
namespace Module\KPI\Traits;

use Module\KPI\Models\KPIModel;
use Module\KPI\Models\KPIQuestion;

trait KPIQuestionTrait
{

	protected function kpiGroupList(){
		$data = KPIModel::where('stat', 1)->get();
		return $data;
	}


	public function questionData($idkpi=0){
		$data = KPIQuestion::where('stat', 1)
			->where('id_kpi_group', $idkpi)
			->orderBy('ord')
			->get();

		return view('kpi::partials.table', compact('data'))->render();
	}

	protected function validation(){
		return \Validator::make($this->request->all(), [
			'statement' => 'required',
			'bobot' => 'required|integer'
		]);
	}

}