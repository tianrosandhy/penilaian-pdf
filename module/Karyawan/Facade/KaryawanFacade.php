<?php 

namespace Module\Karyawan\Facade;

use Illuminate\Support\Facades\Facade;

class KaryawanFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Karyawan\\Controllers\\KaryawanController';
    }
}