<div align="center">
	<h1>{{ isset($title) ? $title : '' }}</h1>
</div>
<hr>

<table>
	<tr>
		<td>Periode</td>
		<td>:</td>
		<td>{{ date('F Y', strtotime($data->periode)) }}</td>
	</tr>
	<tr>
		<td>Divisi</td>
		<td>:</td>
		<td>{{ $data->divisi->title }}</td>
	</tr>
	<tr>
		<td>Nama Karyawan</td>
		<td>:</td>
		<td>{{ $data->karyawan->title }}</td>
	</tr>
	<tr>
		<td>Penilai</td>
		<td>:</td>
		<td>{{ $data->penilai->username }}</td>
	</tr>
</table>


<br><br>

<table  style="width:100%;" border="1" cellspacing="0">
	<thead>
		<tr>
			<th>Statement</th>
			<th>Score</th>
			<th>Bobot</th>
			<th>Info</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$total = [
			'skor' => 0,
			'bobot' => 0,
			'index' => 0
		];
		?>
		@foreach($score as $row)
		<tr>
			<td>{{ $row->question->statement }}</td>
			<td>{{ $row->value }} / {{ $row->question->max_threshold }}</td>
			<td><?php
			$skor = ($row->value - $row->question->min_threshold) / ($row->question->max_threshold - $row->question->min_threshold) * $row->question->bobot;
			echo '<strong>'.$skor .' / '. $row->question->bobot.'</strong>';

			if($row->question->bobot > 0){
				$index = ($skor / $row->question->bobot * 100);
				$total['skor'] += $skor;
				$total['bobot'] += $row->question->bobot;
				$total['index'] += ($index / 100);
			}
			else{
				$index = 0;
			}
			?></td>
			<td>
				<em><small>
				@if($index > 80)
				Sangat Baik
				@elseif($index > 60)
				Baik
				@elseif($index > 40)
				Standar
				@elseif($index > 20)
				Kurang
				@else
				Sangat Kurang
				@endif
				</small></em>
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td></td>
			<td></td>
			<td><h4>{{ $total['skor'] }} / {{ $total['bobot'] }}</h4></td>
			<td></td>
		</tr>
	</tfoot>
</table>