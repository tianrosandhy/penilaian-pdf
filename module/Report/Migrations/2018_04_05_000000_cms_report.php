<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('cms_report', function(Blueprint $tb){
            $tb->increments('id');
            $tb->integer('id_karyawan');
            $tb->integer('id_divisi');
            $tb->integer('id_penilai');
            $tb->date('periode');
            $tb->double('skor', 5, 2);
            $tb->integer('bobot')->nullable();
            $tb->double('index', 5, 2)->nullable();
            $tb->double('kpi', 5, 2)->nullable();
            $tb->timestamps();
            $tb->tinyinteger('stat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('cms_report');
    }
}
