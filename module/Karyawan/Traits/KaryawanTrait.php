<?php
namespace Module\Karyawan\Traits;

use Form;
use App\Model\DataTableModel;
use Request;
use Module\Karyawan\Models\KaryawanModel;

use App\Http\Controllers\Admin\Api\DatatableApi;

trait KaryawanTrait
{

	//Structure Trait

	public function register_column(){
		$this->columns = [
			'Nama' => ['search' => true, 'col' => 'title'],
			'Tempat' => ['search' => true, 'col' => 'tempat'],
			'Tgl Lahir' => ['search' => true, 'col' => 'tgl_lahir'],
			'JK' => ['search' => true, 'col' => 'jk', 'custom' => true],
			'Divisi' => ['search' => true, 'col' => 'divisi', 'custom' => true],
			'Status'	=> ['search' => false, 'col' => 'stat'],
			'' => ['search' => false, 'col' => 'button'], 
		];
	}

    //
    public function register_form(){
		$this->forms = [
			'Nama Karyawan' => Form::input('text', 'title', old('title', null), ['class' => 'form-control']),

			'Divisi' => Form::select('divisi', get_list('cms_divisi', 'title'), old('divisi'), ['class' => 'form-control']),

			'Jabatan' => Form::input('text', 'jabatan', old('jabatan'), ['class' => 'form-control']),

			'Tempat' => Form::input('text', 'tempat', old('tempat'), ['class' => 'form-control']),
			'Tgl Lahir' => Form::input('text', 'tgl_lahir', old('tgl_lahir'), ['class' => 'form-control datepicker']),

			'JK' => Form::select('jk', [1 => 'Pria', 2 => 'Wanita'], old('jk'), ['class' => 'form-control']),

			'Email' => Form::input('email', 'email', old('email'), ['class' => 'form-control']),
			'Telp' => Form::input('tel', 'telp', old('telp'), ['class' => 'form-control']),
			'No HP' => Form::input('tel', 'hp', old('hp'), ['class' => 'form-control']),
			
			'Image Karyawan' => '
			<div class="row">
				<div class="col-sm-6">
					<input type="hidden" name="uploaded_img" value="" class="dropzone_uploaded"><div class="dropzone mydropzone" data-target="'.env('ADMIN_PREFIX').'/api/gallery">
					</div>				
				</div>
				<div class="col-sm-6">
					<div class="append">
						<strong>Current Image Uploaded</strong>
						<br>
						<img src="">
					</div>
				</div>
			</div>
			',
			
			'Description' => Form::textarea('description', old('description', null), ['class' => 'editor',  'id' => 'editor']),
			'Status' => Form::select('stat', [1 => 'Live', 0 => 'Draft'], old('stat', null), ['class'=>'form-control']),
		];    	
    }



    //Data Table Trait

    public function getPost(){
    	$request = $this->request->all();

    	$dt = new DatatableApi($request, KaryawanModel::with('getDivisi'), $this->columns);
    	$output = $dt->generate(true);
    	$output['data'] = $this->table_format($request, $output);
    	//hapus query result mentah
    	unset($output['result']);

    	return json_encode($output);
    }

    public function table_format($data, $output){
    	if(count($output['result']) == 0){
			$ret = $output['data'];
		}
		else{
			foreach($output['result'] as $row){
				//field index disesuaikan dengan parameter col di this->column
				$exp = explode(",", $row->tags);
				$tags = "";
				foreach($exp as $ex){
					$tags .= '<span class="label label-primary">'.trim($ex)."</span> ";
				}

				$btn = '';

				if(hasAccess('admin.karyawan.edit')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>';
				}
				if(hasAccess('admin.karyawan.destroy')){
					$btn .= '<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>';
				}

				$ret[] = array(
					$row->title,
					$row->tempat,
					$row->tgl_lahir,
					($row->jk == 1) ? 'Pria' : 'Wanita',
					(isset($row->getDivisi->title) ? $row->getDivisi->title : ''),
					'<input type="checkbox" data-init="switchery" data-size="small" class="js-switch" data-id="'.$row->id.'" '.($row->stat == 1 ? 'checked' : '').'>',
					'<div class="btn-group">
						'.$btn.'
					</div>'
				);
			}
		}
		return $ret;
	}




	//switch trait
	public function getSwitch(){
		if($this->request->has(['id','stat'])){
			$id = intval($this->request->id);
			$stat = intval($this->request->stat);

			$data = $this->model->where('id', $id)->get();
			if(count($data) > 0){
				//data ada, bisa diupdate
				$this->model
					->where('id', $id)
					->update(['stat'=> $stat]);

				$out['success'] = "Berhasil mengupdate status post";
			}
			else{
				//data tidak ada. tidak bisa diupdate
				$out['error'] = "Post tidak ditemukan";
			}

		}
		else{
			$out['error'] = "Invalid operation";
		}
		return json_encode($out);
	}
}