<?php
//CMS Helper Class




//VIEW CSS + JS HELPER SECTION

function assets($target){
	return url(config('asset.asset_dir').$target);
}

function register_single($filename, $type){
	if($type == "css"){
		return '<link rel="stylesheet" href="'.(assets($filename)).'" type="text/css">
	';
	}
	else if($type == "js"){
		return '<script type="text/javascript" src="'.(assets($filename)).'"></script>
	';
	}
}






function admin_url($url){
	$prefix = env('ADMIN_PREFIX');
	$real_target = $prefix."/".$url;
	return $real_target;
}

function indo_date($tgl, $type="full"){
	//shiet
	$tgl = date("Y-m-d H:i:s", strtotime($tgl));

	$int = strtotime($tgl);
	$tahun = date("Y",$int);
	$bulan = list_bulan(date("n",$int));
	$tanggal = date("d",$int);

	$fullDate = "$tanggal $bulan $tahun";

	if($type <> "half"){
		$jam = date("H:i:s", $int);
		return $fullDate." ".$jam;
	}
	return $fullDate;
}

function list_bulan($i=0, $mode="get"){
	$bulan = ['','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

	if($mode=="get"){
		return $bulan[$i];
	}
	return $bulan;
}

function file_get($filename){
	$pecah = explode(".", $filename);
	$n = count($pecah) - 1;

	$out['extension'] = $pecah[$n];
	unset($pecah[$n]);
	$out['filename'] = implode(".",$pecah);
	return $out;
}

function slug($input){
	$txt = strtolower($input);
	$pch = explode(" ",$txt);
	return implode("-", $pch);
}

function get_list($tbname, $field){
	$sql = DB::table($tbname)
	->where('stat','<>',9)
	->get();

	$row = array();
	foreach($sql as $r){
		$r = (array)$r;
		$row[$r['id']] = $r[$field];
	}
	return $row;
}


function ajax_response($msg, $type='error', $additional=[]){
	$arr = [
		'type' => $type,
		'message' => $msg
	];

	if(count($additional) > 0){
		$arr = $arr + $additional;
	}

	return $arr;
}


function current_user(){
	return Auth::user();
}

function hasAccess($routename=''){
	if(strlen($routename) == 0)
		return true;

	$priviledge = current_user();
	$list = isset($priviledge->getPriviledge->permission) ? $priviledge->getPriviledge->permission : '[]';
	$data = json_decode($list, true);

	$shortroute = str_replace('admin.', '', $routename);
	if(in_array($routename, $data) || in_array($shortroute, $data)){
		return true;
	}

	return false;
}