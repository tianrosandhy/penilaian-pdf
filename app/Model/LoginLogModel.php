<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoginLogModel extends Model
{
    protected $table = 'cms_login_log';
    protected $fillable = [
        'id', 'ip', 'server_info', 'username', 'created_at', 'updated_at', 'stat'
    ];



}
