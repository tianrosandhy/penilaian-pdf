<?php 

namespace Module\Divisi\Facade;

use Illuminate\Support\Facades\Facade;

class DivisiFacade extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Module\\Divisi\\Controllers\\DivisiController';
    }
}