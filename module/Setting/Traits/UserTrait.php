<?php
namespace Module\Setting\Traits;

use Form;
use App\Model\DataTableModel;
use Request;

use Module\Setting\Models\UserModel;

use App\Http\Controllers\Admin\Api\DatatableApi;

trait UserTrait
{

	//Structure Trait

	public function register_column(){
		$this->columns = [
			'Username' => ['search' => true, 'col' => 'username'],
			'Email' => ['search' => true, 'col' => 'email'],
			'Created At' => ['search' => true, 'col' => 'created_at'],
			'Priviledge' => ['search' => false, 'col' => 'priviledge'],
			'' => ['search' => false, 'col' => 'button'], 
		];
	}

    //
    public function register_form(){
		$this->forms = [
			'Username' => Form::input('text', 'username', old('username', null), ['class' => 'form-control']),

			'Email' => Form::input('email', 'email', old('email', null), ['class' => 'form-control']),

			'Password' => Form::input('password', 'pass', old('pass1', null), ['class' => 'form-control']),

			'Repeat Password' => Form::input('password', 'pass_confirmation', old('pass2', null), ['class' => 'form-control']),

			'Priviledge' => Form::select('priviledge', get_list('users_priviledge', 'priviledge_name'), old('priviledge', null), ['class' => 'form-control']),

		];    	
    }



    //Data Table Trait

    public function getPost(){
    	$request = $this->request->all();

    	$dt = new DatatableApi($request, new UserModel(), $this->columns);
    	$output = $dt->generate(false);
    	$output['data'] = $this->table_format($request, $output);
    	//hapus query result mentah
    	unset($output['result']);

    	return json_encode($output);
    }

    public function table_format($data, $output){
    	if(count($output['result']) == 0){
			$ret = $output['data'];
		}
		else{
			foreach($output['result'] as $row){
				//field index disesuaikan dengan parameter col di this->column

				$ret[] = array(
					$row->username,
					$row->email,
					indo_date($row->created_at),
					DataTableModel::get_from_tb('users_priviledge',$row->priviledge, 'priviledge_name'),

					'<div class="btn-group">
						<a data-id="'.$row->id.'" class="btn-edit btn btn-sm btn-primary waves-effect edit-btn" data-toggle="modal" data-target="#modalForm">
							<i class="fa fa-pencil"></i>
						</a>
						<a data-id="'.$row->id.'" class="btn-delete btn btn-sm btn-danger waves-effect delete-btn">
							<i class="fa fa-trash"></i>
						</a>
					</div>'
				);
			}
		}
		return $ret;
	}

}