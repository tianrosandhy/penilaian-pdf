<script>
//Admin autoload script
var ADMIN_URL = "{{ url('/') }}";
var BASE_URL = "{{ url('/') }}";
var UPLOADDIR = "{{ config('filesystems.disks.local.root') }}";

(function($){
	$(function(){
		var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
		
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': CSRF_TOKEN
		    }
		});

	    //sidebar link background
		if($("ul li ul li.active").length > 0){
			$("ul li ul li.active").closest('li.parent').addClass('active');
			$("ul li ul li.active").closest('ul.children').collapse("show");
		}



		/*
		Application Reserved Variable Item : 

		- tb_data		=> DataTable instance
		- edt			=> CKEditor Instance
		- .mydropzone	=> Dropzone Class

		*/


		//default script here
		@if(session('error'))
			@if(is_array(session('error')))
				@foreach(session('error') as $err => $msg)
					sweet_alert('error', '{{ $msg }}', 'error');
				@endforeach
			@else
				sweet_alert('error', '{{ session('error') }}', 'error');
			@endif

		@elseif(session('success'))
			sweet_alert('success', '{{ session('success') }}', 'success');
		@elseif(session('alert'))
			sweet_alert('alert', '{{ session('alert') }}', 'alert');
		@endif


		@if($errors->any())
			@foreach($errors->all() as $err)
				sweet_alert('error', '{{ $err }}', 'error');
			@endforeach
		@endif

		//searchable datatable
		$("table.data .search th").each(function(key, value){
			var title = $(value).text();
			if(title.length > 0){
				$(value).html('<input class="form-control search" id="'+title.replace(' ','_').toLowerCase()+'" type="text" placeholder="Search '+title+'" />');
			}
		});

		$('html').on(
			"change", "table.data .search input, table.data .search select", $.debounce(250, function(){
			tb_data.ajax.reload();
		}));

		$("table.data .search input").on(
			"keyup", $.debounce(250, function(){
			tb_data.ajax.reload();
		}));


		//title automatic slug 
		$(".slug-toggle").on('keyup', $.debounce(100, function(){
	        txt = convertToSlug($(this).val());
	        $(".slug-target").val(txt);
	    }));

		//automatic send data from ajax
		$("form.ajax-form").submit(function(e){
			e.preventDefault();
			form = $(this);

			$.ajax({
				url : form.attr('action'),
				type : form.attr('method'),
				data : form.serialize(),
				dataType : 'json'
			}).done(function(dt){
				console.log(dt);
				if(dt['error']){
					$.each(dt['error'], function(k, v){
						alertify.error(v[0]);
					});
				}

				if(dt['success']){
					alertify.success(dt['success']);
					$("form.ajax-form")[0].reset();
					form_refresh();

					$(".modal.fade").modal('hide');
					tb_data.ajax.reload();
				}
			}).fail(function(dt){
				swal('Error ' + dt.status, dt.responseText, 'warning');
			});
		});

		//editor instance

		if($("#editor").length){
			$("#editor").summernote({
				height : 300,
				callbacks : {
					onImageUpload : function(files, editor, welEdit){
						console.log([files[0], editor, welEditable]);
						sendFile(files[0], editor, welEditable);
					}
				}
			});
		}

		if($("#ckeditor").length){
			load_ckeditor("ckeditor");
		}


		//dropzone instance
		if($(".mydropzone").length){
			Dropzone.autoDiscover = false;
			var ajaxurl = $(".mydropzone").data("target");
			$(".mydropzone").dropzone({
				url : ajaxurl,
				sending : function(file, xhr, formData){
					formData.append("_token", CSRF_TOKEN);
				},
				init : function(){
					this.on("success", function(file, data){
						$(".dropzone_uploaded").val(data);
					});
					this.on("addedfile", function() {
				      if (this.files[1]!=null){
				        this.removeFile(this.files[0]);
				      }
				    });
				}
			});
		}

		if($(".mydropzone2").length){
			Dropzone.autoDiscover = false;
			var ajaxurl = $(".mydropzone2").data("target");
			$(".mydropzone2").dropzone({
				url : ajaxurl,
				sending : function(file, xhr, formData){
					formData.append("_token", CSRF_TOKEN);
				},
				init : function(){
					this.on("success", function(file, data){
						$(".dropzone_uploaded2").val(data);
					});
					this.on("addedfile", function() {
				      if (this.files[1]!=null){
				        this.removeFile(this.files[0]);
				      }
				    });
				}
			});
		}

		//disable tagsinput enter to send
		$("body").on('keypress', '.bootstrap-tagsinput input', function(e){
			if(e.keyCode == 13){
				e.preventDefault();
				return false;
			}
		});


		@yield('custom-script')

	});
})(jQuery);

function load_ckeditor(ckeditor_id){
	var config = {
	    filebrowserUploadUrl: BASE_URL+'/api/gallery/ckeditor',
		extraPlugins: 'codesnippet',
		codeSnippet_theme: 'monokai_sublime',
		height: 356
	};

	CKEDITOR.replace( ckeditor_id, config );

	CKEDITOR.instances[ckeditor_id].on('change', function() { 
		CKEDITOR.instances[ckeditor_id].updateElement();
	});
}

function destroy_ckeditor(ckeditor_id){
	CKEDITOR.instances[ckeditor_id].destroy();
}


function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

function msghandling(data){
	if(data['error'])
		alertify.error(data['error']);
	if(data['success'])
		alertify.success(data['success']);
}

function sweet_alert(type, message){
	swal(type, message, type);
}


function form_refresh(){
    $("[data-role='tagsinput']").tagsinput('destroy');
    $("[data-role='tagsinput']").val('');
    $("[data-role='tagsinput']").tagsinput('refresh');
    $("#editor").summernote('destroy');
    $("#editor").summernote({
		height : 300,
		callbacks : {
			onImageUpload : function(files, editor, welEditable){
				sendFile(files[0], editor, welEditable);
			}
		}
	});

	if($("#ckeditor").length){
		destroy_ckeditor("ckeditor");
		$("#ckeditor").val('');
		load_ckeditor("ckeditor");
	}

	$('#typeahead').tokenfield('setTokens', [], false, false);

	$(".if-updated img").attr('src', '');

    if($('.mydropzone').length)
    Dropzone.forElement(".mydropzone").removeAllFiles(true);
}

function sendFile(file, editor, welEditable) {
	console.log([file, editor, welEditable]);
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url: ADMIN_URL+"/api/gallery",
        cache: false,
        contentType: false,
        processData: false,
    }).done(function(url){
    	console.log(UPLOADDIR+'/'+url);
    	$("#editor").summernote('insertImage', UPLOADDIR+'/'+url);
    });
}
</script>