<?php

namespace Module\KPI\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use App\Model\DataTableModel;
use Form;
use Validator;
use Auth;
use Ticmi;
use App\Model\GalleryModel;

use Module\KPI\Models\KPIModel;
use Module\KPI\Traits\KPITrait;


class KPIController extends Controller
{
	public 	$request, 
			$response,
			$columns,
			$forms,
			$model;
	
	use KPITrait;

    //
	public function __construct(Request $req, Response $red){
		$this->request = $req;
		$this->response = $red;
		$this->model = new KPIModel();
		
		$this->register_column();
	}

    public function index(){
		$this->register_form();

    	return view('kpi::index')->with([
    		'title' => 'KPI',
    		'columns' => $this->columns,
    		'forms' => $this->forms,
    		'datatable_search' => DataTableModel::generate_search($this->columns),
    		'datatable_ajax' => [
    			'tb' => '/kpi/table',
    			'switch' => '/kpi/switch',
    			'edit' => '/kpi/edit',
    			'delete' => '/kpi/destroy'
    		],
    		'edit_action' => route('admin.kpi.data.update'),
    		'jk_box' => DataTableModel::manualComboBox('jk', [
    			1 => 'Pria',
    			2 => 'Wanita'
    		])
    	]);

    }

    
    public function store(){
    	$rule = [
    		'title' => 'required|max:255',
    		'divisi' => 'array|required'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('stat', '<>', 9)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data kpi dengan nama tersebut sudah ada. Mohon gunakan nama lain agar mudah dibedakan";

		}
		else{
            $stat = strlen($this->request->live) > 0 ? 1 : 0; 
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$instance = $this->model->create([
				'title' => $this->request->title,
				'description' => $this->request->description,
				'ord' => intval($this->request->ord),
				'stat' => intval($this->request->stat)
			]);

			self::storeKPIDivisi($instance->id);

			$out['success'] = "Berhasil menyimpan data kpi";
		}

    	return json_encode($out);

    }

    public function update($id){
    	$rule = [
    		'title' => 'required|max:255',
    		'divisi' => 'required|array'
    	];
    	$validate = Validator::make($this->request->all(), $rule);
    	if($validate->fails()){
			foreach ($validate->messages()->getMessages() as $field_name => $messages)
			{
			    $out['error'][] = $messages;
			}
			return json_encode($out);
    	}

		//aman, lanjut cek selanjutnya
		$cek = $this->model
			->where('title', $this->request->title)
			->where('stat', '<>', 9)
			->where('id','<>', $id)
			->get();

		if(count($cek) > 0){
			$out['error'] = "Data KPI Group dengan judul tersebut sudah ada.";

		}
		else{
            $slug = strlen($this->request->slug) > 0 ? $this->request->slug : slug($this->request->title);
			//proses simpan
			$list_update = [
				'title' => $this->request->title,
				'description' => $this->request->description,
				'stat' => $this->request->stat,
			];

			$this->model->where('id', $id)->update($list_update);
			self::storeKPIDivisi($id);

			$out['success'] = "Berhasil mengupdate data kpi";
		}

    	return json_encode($out);
    }


    public function edit(){
    	if($this->request->has('id')){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;
	    		$cek = $this->model->with('list')->find($id);
	    		if($cek){
	    			$out = $cek->toArray();
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
	    }
    }

    public function destroy(){
    	if($this->request->ajax()){
	    	if($this->request->has('id')){
	    		$id = $this->request->id;

	    		$cek = $this->model->find($id);
	    		if($cek){
	    			$delimg = $cek->image;
	    			GalleryModel::rollback($delimg);

		    		$cek->delete();
		    		$out['success'] = "Berhasil menghapus data";
	    		}
	    		else{
	    			$out['error'] = "Data not found";
	    		}
	    	}
	    	else{
	    		$out['error'] = "Invalid operation";
	    	}
	    	return json_encode($out);
    	}
    }

    static function getByName($name,  $default=''){
    	//get KPI URL by name
    	$sql = KPIModel::where('title', $name)
    		->where('stat', 1)
    		->first();
    	if(count($sql) > 0){
    		return url('upload/'.$sql->image);
    	}
    	return $default;
    }
}
