<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use Auth;
use App\Model\LoginModel;
use App\Model\LoginLogModel;


class DashboardController extends Controller
{
    //
    var $request;

    public function __construct(Request $request){
    	$this->middleware('login');
    	$this->request = $request;
    }

    public function index(){
    	return view('admin.dashboard')->with([
    		
    	]);
    }

    public function login(){
        if(Auth::user()){
           return redirect()->route('admin.dashboard'); 
        }
    	return view('admin.login');
    }

    public function loginProcess(){
    	$post = $this->request->all();
    	$login = new LoginModel($post);

    	$attempt = $login->attempt();
    	if(isset($attempt['success'])){
            $this->clear_log_attempt();
    		return redirect()->route('admin.dashboard');
        }
        elseif(isset($attempt['info'])){
            return redirect()->route('admin.login')->with(['error' => $attempt['info']]);
        }
    	else{
            //record failed login
            $this->record_log_attempt();
    		return redirect()->route('admin.login')->with($attempt);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.dashboard')->with([
            'success' => 'Anda sudah logout dari sistem'
        ]);
    }

    public function record_log_attempt($stat='fail'){
        $ip = $_SERVER['REMOTE_ADDR'];
        $username = $this->request->username;
        $server_info = $_SERVER['HTTP_USER_AGENT'];
        $now = date('Y-m-d H:i:s');

        $x = LoginLogModel::insert([
            'ip' => $ip,
            'server_info' => $server_info,
            'username' => $username,
            'created_at' => $now,
            'stat' => $stat,
        ]);
        return true;
    }

    public function clear_log_attempt($stat='cleared'){
        $ip = $_SERVER['REMOTE_ADDR'];
        $username = $this->request->username;

        LoginLogModel::where('ip', $ip)
            ->where('username', $username)
            ->update([
                'stat' => $stat
            ]);

        return true;
    }
}
