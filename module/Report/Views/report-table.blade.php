@foreach($data->unique('id_karyawan') as $row)
<?php
	$penilai = $row->pluck('id_penilai')->unique();
	$rowspan = count($penilai);
?>
<tr>
	<td rowspan='{{ $rowspan }}'>{{ $row->karyawan->title }}</td>
	<td rowspan='{{ $rowspan }}'>{{ $row->divisi->title }}</td>
	<td rowspan='{{ $rowspan }}'>{{ $row->karyawan->jabatan }}</td>
	<?php
	$kesimpulan = 0;
	?>
	@foreach($penilai as $idpenilai)
		@if(!$loop->first)
		<tr>
		@endif
		<td>{{ $row->penilai->where('id', $idpenilai)->first()->username }}</td>
		<?php
		$temp = $row->where('id_penilai', $idpenilai)
					->where('id_karyawan', $row->id_karyawan)
					->where('periode', $row->periode)
					->first();

		$group = $row->where('periode', $row->periode)
					->where('id_karyawan', $row->id_karyawan)
					->get();

		$skor = isset($temp->skor) ? $temp->skor : 0;
		$bobot = isset($temp->bobot) ? $temp->bobot : 0;
		$width = ($bobot > 0) ? ($skor / $bobot * 100) : 0;
		?>
		<td>
			@if($bobot > 0)
				{{ $skor }} / {{ $bobot }}
				@if(!$print)
				<span>
					<div class="progress">
						<div class="progress-bar progress-bar-complete" style="width:{{ $width }}%"></div>
					</div>
				</span>
				@endif
			@else
			<small class="label label-danger">{{ $print ? '-' : 'belum mengisi nilai' }}</small>
			@endif
		</td>
		<td>{!! isset($temp->index) ? $print ? $temp->index : '<a href="'.admin_url('report/detail/'.$temp->id).'" class="btn btn-primary" title="Detail"><i class="fa fa-eye"></i> '.$temp->index.'</a>' : '' !!}</td>
		@if($loop->first)
		<td rowspan="{{ $rowspan }}">{{ $row->kpi }}</td>
		@else
		</tr>
		@endif

	@endforeach
</tr>
@endforeach