<?php

namespace Module\Setting\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use Response;
use App\Model\DataTableModel;
use Module\Setting\Models\SettingModel;
use Form;
use Validator;
use Auth;
use Module\Setting\Traits\SettingTrait;


class SettingController extends Controller
{
	public 	$request, 
			$response,
			$columns,
			$forms,
			$model;
	
	use SettingTrait;

    //
	public function __construct(Request $req, Response $red){
		$this->request = $req;
		$this->response = $red;
		$this->model = new SettingModel();
	}

    public function index(){
    	$data = config('setting');
    	$filled = $this->model->where('stat', '<>', 9)->get()->pluck('value', 'param')->toArray();

    	return view('setting::general')->with([
    		'title' => 'General Setting',
    		'form' => $data,
    		'filled' => $filled
    	]);
    }

    public function store(){
    	$post = $this->request->all();
    	unset($post['_token']);
    	foreach($post as $key => $value){
    		$cek = $this->model->where('param', $key)->get();
			self::setParam($key, $value, true);
    	}

    	return redirect(route('admin.setting.setting.index'))->with([
    		'success' => 'Setting has been updated'
    	]);
    }

}
